The PSS CI Systems Repository
=============================

The CI Systems repository contains the scripts and supporting tools for deploying Cheetah_: a pulsar and fast-transient search application.

This documentation contains a user guide for those simply wanting to deploy Cheetah and a developer guide for the installation scripts and supporting tools.

.. _Cheetah: https://ska-telescope.gitlab.io/pss/ska-pss-cheetah

..
   image:: images/protest.png

.. toctree::
  :maxdepth: 1

  user_guide
  developer_guide