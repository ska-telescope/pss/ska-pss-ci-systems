#!/usr/bin/env python3
import datetime

now = datetime.datetime.now()
result = 60 - now.second
if now.minute % 2 == 1:
    result += 60
print(result)
