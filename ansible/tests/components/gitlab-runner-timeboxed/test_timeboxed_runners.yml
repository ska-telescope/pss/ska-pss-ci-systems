---
- name: Test time-boxed runner
  hosts: "{{ container_name }}"
  connection: docker
  roles: [curl]  # We need curl for the testing steps
  vars:
    project_export_name: "ci-test-project-export.tar.gz"
    project_export_dest_path: "/tmp/{{ project_export_name }}"
    project_name: "ci-test-project"
  pre_tasks:
    - name: Run tasks for setting gitlab_host
      ansible.builtin.include_tasks: ../../../roles/gitlab-runner/tasks/set_test_gitlab_host.yml
  tasks:
    # We need to start the crond daemon manually if we're in
    # a Docker container on RHEL
    - name: Run crond daemon if in container on RHEL
      become: true
      ansible.builtin.shell: crond
      when: >
        ansible_os_family|lower == 'redhat' and
        ansible_facts['virtualization_type']|lower == 'docker'

    - name: Copy project export file to test container
      ansible.builtin.copy:
        src: "{{ project_export_name }}"
        dest: "{{ project_export_dest_path }}"

    - name: Import CI test project to GitLab instance
      ansible.builtin.shell:
        cmd: >
          curl --request POST --header "PRIVATE-TOKEN: {{ api_token }}"
          --form "path={{ project_name }}" --form "file=@{{ project_export_dest_path }}"
          "{{ gitlab_host }}/api/v4/projects/import"
      register: project_import_result

    - name: Delete project export file from test container (not needed anymore)
      ansible.builtin.file:
        path: "{{ project_export_dest_path }}"
        state: absent

    - name: Save project ID
      ansible.builtin.set_fact:
        project_id: "{{ (project_import_result.stdout | from_json).id }}"

    - name: Create trigger for CI for imported project
      ansible.builtin.uri:
        url: "{{ gitlab_host }}/api/v4/projects/{{ project_id }}/triggers"
        method: POST
        body:
          description: "Trigger for gitlab-runner-timeboxed component test"
        body_format: form-multipart
        status_code: [200, 201]
        headers:
          PRIVATE-TOKEN: "{{ api_token }}"
      register: ci_trigger_result

    - name: Save CI trigger token
      ansible.builtin.set_fact:
        trigger_token: "{{ ci_trigger_result.json.token }}"

    - name: Get seconds until next odd minute
      ansible.builtin.script: seconds_to_odd_minute.py
      register: seconds_until_odd_minute

    - name: Wait for next odd minute
      ansible.builtin.wait_for:
        timeout: "{{ seconds_until_odd_minute.stdout|int }}"

    # This is done to allow a bit of time for the cron job for disabling the runner to kick in
    - name: Wait for 5 seconds before triggering test CI pipeline
      ansible.builtin.wait_for:
        timeout: 5

    - name: Trigger CI pipeline for test project
      ansible.builtin.shell:
        cmd: >
          curl -X POST
          -F token={{ trigger_token }}
          -F ref=master
          {{ gitlab_host }}/api/v4/projects/{{ project_id }}/trigger/pipeline
      register: pipeline_trigger_result

    - name: Save pipeline ID
      ansible.builtin.set_fact:
        pipeline_id: "{{ (pipeline_trigger_result.stdout | from_json).id }}"

    - name: Wait for 5 seconds before checking pipeline status
      ansible.builtin.wait_for:
        timeout: 5

    - name: Get pipeline status
      ansible.builtin.uri:
        url: "{{ gitlab_host }}/api/v4/projects/{{ project_id }}/pipelines/{{ pipeline_id }}"
        method: GET
        headers:
          PRIVATE-TOKEN: "{{ api_token }}"
      register: pipeline_status

    - name: Get the pipeline's job IDs
      ansible.builtin.uri:
        url: "{{ gitlab_host }}/api/v4/projects/{{ project_id }}/pipelines/{{ pipeline_id }}/jobs"
        method: GET
        headers:
          PRIVATE-TOKEN: "{{ api_token }}"
        return_content: yes
      register: pipeline_job_list

    - name: Get pipeline job trace
      ansible.builtin.uri:
        url: "{{ gitlab_host }}/api/v4/projects/{{ project_id }}/jobs/{{ pipeline_job_list.json[0].id }}/trace"
        method: GET
        headers:
          PRIVATE-TOKEN: "{{ api_token }}"
        return_content: yes
      register: pipeline_job_trace

    - name: "[DEBUG] Show pipeline job trace"
      ansible.builtin.debug:
        var: pipeline_job_trace

    - name: Fail if pipeline status is not pending
      ansible.builtin.fail:
        msg: "The pipeline should be pending but was actually {{ pipeline_status.json.status }}"
      when: pipeline_status.json.status != 'pending'

    - name: Get seconds until next even minute
      ansible.builtin.script: seconds_to_even_minute.py
      register: seconds_until_even_minute

    - name: Wait for next even minute
      ansible.builtin.wait_for:
        timeout: "{{ seconds_until_even_minute.stdout|int }}"

    - name: Wait for 5 seconds before checking pipeline status
      ansible.builtin.wait_for:
        timeout: 5

    - name: Get pipeline status
      ansible.builtin.uri:
        url: "{{ gitlab_host }}/api/v4/projects/{{ project_id }}/pipelines/{{ pipeline_id }}"
        method: GET
        headers:
          PRIVATE-TOKEN: "{{ api_token }}"
      register: pipeline_status

    - name: Get pipeline job trace
      ansible.builtin.uri:
        url: "{{ gitlab_host }}/api/v4/projects/{{ project_id }}/jobs/{{ pipeline_job_list.json[0].id }}/trace"
        method: GET
        headers:
          PRIVATE-TOKEN: "{{ api_token }}"
        return_content: yes
      register: pipeline_job_trace
    
    - name: "[DEBUG] Show pipeline job trace"
      ansible.builtin.debug:
        var: pipeline_job_trace

    - name: Fail if pipeline status is not running or success
      ansible.builtin.fail:
        msg: "The pipeline's status should be running or success but was actually {{ pipeline_status.json.status }}"
      when: pipeline_status.json.status != 'running' and pipeline_status.json.status != 'success'

    - name: Wait until CI job has finished
      ansible.builtin.uri:
        url: "{{ gitlab_host }}/api/v4/projects/{{ project_id }}/pipelines/{{ pipeline_id }}"
        method: GET
        headers:
          PRIVATE-TOKEN: "{{ api_token }}"
      register: pipeline_status
      until: pipeline_status.json.status == 'success'
      retries: 120
      delay: 1
      changed_when: false

    - name: Delete the test project
      ansible.builtin.uri:
        url: "{{ gitlab_host }}/api/v4/projects/{{ project_id }}"
        method: DELETE
        status_code: [200, 202]
        headers:
          PRIVATE-TOKEN: "{{ api_token }}"

    - name: Delete /builds directory that was created by the CI job
      become: true
      ansible.builtin.file:
        path: /builds
        state: absent
