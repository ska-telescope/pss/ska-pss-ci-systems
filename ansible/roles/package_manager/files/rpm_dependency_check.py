#!/usr/bin/env python3
import argparse
import shutil
import subprocess
import sys
import textwrap

def ensure_rpm():
    """
    Ensures that the rpm program exists and can be run. If it isn't, an
    error message is printed and the program exits with a return code
    of 1.
    """
    if not shutil.which('rpm'):
        print(
            '[ERROR] Couldn\'t run rpm command. The rpm command is needed '
            'for this script to function.'
        )
        sys.exit(1)

def get_reverse_dependencies(package):
    """
    Gets the reverse dependencies of an individual rpm package by using
    the rpm 'whatrequires' query.

    :param package: The package to retrieve reverse dependencies for
    :type package: str
    :raises subprocess.CalledProcessError: If an unknown error occurred
        when running the rpm command
    :return: A set containing the found reverse dependencies, if any
    :rtype: Set of strs
    """
    rpm_command = subprocess.run(
        ['rpm', '-q', '--whatrequires', package, '--qf', '%{NAME}\n'],
        stdout=subprocess.PIPE)

    # Return either nothing or stdout depending on return code
    if rpm_command.returncode == 1:
        return set()
    if rpm_command.returncode == 0:
        result = set()
        for line in rpm_command.stdout.splitlines():
            result.add(line.decode())
        return result

    # If the return code wasn't 0 or 1, raise an exception
    raise subprocess.CalledProcessError()

def check_reverse_dependencies(packages):
    """
    Checks if the provided list of package(s) have any reverse
    dependencies. Appropriate messages are printed out depending on
    whether the package(s) are safe to remove or not. Note that if any
    of the packages in the provided list depend on each other, this is
    ignored and doesn't flag an error.

    :param packages: The list of rpm packages to check reverse
        dependencies for
    :type packages: List of strs
    """
    found_reverse_dependencies = set()
    for package in packages:
        found_reverse_dependencies = found_reverse_dependencies.union(
            get_reverse_dependencies(package))
    found_reverse_dependencies = found_reverse_dependencies - set(packages)

    found_reverse_dependencies = list(found_reverse_dependencies)
    found_reverse_dependencies.sort()

    if found_reverse_dependencies:
        print(textwrap.fill(
            f"If the packages ({', '.join(packages)}) are removed, the "
            'following packages that '
            'depend on them would also be removed. Please remove these reverse '
            'dependencies manually first.') + '\n', file=sys.stderr)
        for dependency in found_reverse_dependencies:
            print(f"- {dependency}", file=sys.stderr)
        print('')
        sys.exit(100)

    if len(packages) == 1:
        print(textwrap.fill(
            f"Package '{packages[0]}' is safe to remove as there are no "
            'other packages installed that depend on it.'
        ))
    else:
        print(textwrap.fill(
            f"It is safe to remove the packages ({', '.join(packages)}) in "
            'one go as there are no other packages installed that depend on '
            'any of them (although they may depend on each other).'
        ))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='RPM reverse dependency checker')
    parser.add_argument('package', type=str, nargs='+')
    args = parser.parse_args()
    ensure_rpm()
    check_reverse_dependencies(args.package)
