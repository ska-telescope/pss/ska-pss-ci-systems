#!/usr/bin/env python3
import apt
import argparse
import sys
import textwrap


DEPENDENCY_TYPE = 'Depends'


def check_reverse_dependencies(packages):
    """
    Checks if the provided package has any installed "reverse
    dependencies" - packages that would be removed if the package is
    removed because they depend on it.

    The different program exit codes are described below:
    0:   No found reverse dependencies - the package is therefore safe
         to remove.
    100: There are installed reverse dependencies of the package - the
         package is therefore NOT safe to remove.

    :param package: The name of the package to check reverse
        dependencies for
    :type package: str
    """
    if not packages:
        print('Nothing to do.')
        sys.exit(1)

    cache = apt.cache.Cache()
    for package in packages:
        try:
            package_from_cache = cache[package]
        except KeyError as err:
            print(err, file=sys.stderr)
            sys.exit(0)

        found_reverse_dependencies = set()
        for other_package in cache:
            # We only want to look at other packages that are installed
            # AND aren't in the list of packages that the user provided
            if (other_package.is_installed and
                    other_package.shortname not in packages):
                for deps in other_package.candidate.get_dependencies(
                        DEPENDENCY_TYPE):
                    for or_dep in deps.or_dependencies:
                        if or_dep.name == package_from_cache.shortname:
                            found_reverse_dependencies.add(
                                other_package.shortname)

        found_reverse_dependencies = list(found_reverse_dependencies)
        found_reverse_dependencies.sort()
        if found_reverse_dependencies:
            print(textwrap.fill(
                f"If package '{package}' is removed, the following packages that "
                'depend on it would also be removed. Please remove these reverse '
                'dependencies manually first.') + '\n', file=sys.stderr)
            for dependency in found_reverse_dependencies:
                print(f"- {dependency}", file=sys.stderr)
            print('')
            sys.exit(100)
    if len(packages) == 1:
        print(textwrap.fill(
            f"Package '{packages[0]}' is safe to remove as there are no "
            'other packages installed that depend on it.'
        ))
    else:
        print(textwrap.fill(
            f"It is safe to remove the packages ({', '.join(packages)}) in "
            'one go as there are no other packages installed that depend on '
            'any of them (although they may depend on each other).'
        ))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Package reverse dependencies checker')
    parser.add_argument(
        'package',
        help='The package to check reverse dependencies for. You can specify '
             'multiple, space-separated packages if you wish.',
        nargs='+')
    args = parser.parse_args()
    check_reverse_dependencies(args.package)
    sys.exit(0)
