---
- name: Check correct version of gcc installed
  become: true
  ansible.builtin.shell: gcc --version | grep {{gcc_version_required}}
  when: ansible_facts['virtualization_type']|lower != 'docker'

# A prerequisite of installing an nvidia driver is to disable the Nouveau drivers on the machine.
# In order to do this we need to blacklist Nouveau.

# Below we create a blacklist file only if one doesn't exist and only add lines to the file to blacklist it
# if they were not already present. We add notes generated by ansible to keep track of the changes we made
# in order to rollback them back.

- name: Check if blacklist file exists
  become: true
  stat:
    path: /etc/modprobe.d/blacklist-nouveau.conf
  register: stat_result
  when: ansible_facts['virtualization_type']|lower != 'docker'

- name: If blacklist file does not already exist create it
  become: true
  ansible.builtin.lineinfile:
    path: /etc/modprobe.d/blacklist-nouveau.conf
    regexp: '# Created'
    line: '# Created by ska-pss-ci-systems'
    create: yes
  when: ansible_facts['virtualization_type']|lower != 'docker' and not stat_result.stat.exists

# This will always run and the condition of true is to prevent errors if the file doesn't exist
- name: Check if blacklist file already has Nouveau blacklisted
  become: true
  ansible.builtin.shell: grep -c "blacklist-nouveau" /etc/modprobe.d/blacklist-nouveau.conf || true
  register: test_grep
  when: ansible_facts['virtualization_type']|lower != 'docker'

- name: Create blacklist file and blacklist Nouveau
  become: true
  ansible.builtin.blockinfile:
    path: /etc/modprobe.d/blacklist-nouveau.conf
    block: |
      blacklist nouveau
      options nouveau modeset=0
  when: ansible_facts['virtualization_type']|lower != 'docker' and test_grep.stdout == "0"

- name: Regenerate the kernel initramfs
  become: true
  ansible.builtin.shell: update-initramfs -u
  when: ansible_facts['virtualization_type']|lower != 'docker'

- name: Reboot machine
  become: true
  ansible.builtin.reboot:
  when: ansible_facts['virtualization_type']|lower != 'docker'

# We use Docker containers for automated testing however the Nvidia Driver can't be installed in a container. The rest of the modules
# in this file are not run if containers are being used. However it is useful to test that the file we want to install is available
# therefore we run this module even when we are in a docker container so it will be included in our testing.
- name: Get Nvidia driver for Ubuntu
  become: true
  ansible.builtin.get_url:
    url: https://us.download.nvidia.com/tesla/{{nvidia_driver_version_22_04}}/NVIDIA-Linux-x86_64-{{nvidia_driver_version_22_04}}.run
    dest: /usr/local/
    mode: 744
    validate_certs: false

- name: Install Nvidia driver for Ubuntu
  become: true
  ansible.builtin.shell: cd /usr/local && /usr/local/NVIDIA-Linux-x86_64-{{nvidia_driver_version_22_04}}.run {{driver_install_options}}
  when: ansible_facts['virtualization_type']|lower != 'docker'