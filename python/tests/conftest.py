import os
import pytest
import sys

import iac_deployer.definitions as defs


@pytest.fixture(scope='session', autouse=True)
def unit_tests_variable():
    """
    Sets the unit tests variable to True to signal that unit tests are
    being run.
    """
    defs.UNIT_TESTS = True


@pytest.fixture(scope='session', autouse=True)
def python_paths():
    """
    Sets the Python paths to be derived from the Python binary used to
    run these unit tests. This is different from the default of the
    Python binary from the virtual environment. Instead, the binary
    in use will be the binary from the .tox directory.
    """
    defs.PythonPaths.initialise(sys.executable)


@pytest.fixture(scope='session', autouse=True)
def ansible_paths():
    """
    Sets the root Ansible directory to tests/data/ansible/ so test data
    is used instead of the real Ansible data.

    This fixture has autouse set to True so any tests will use the test
    ansible directory by default.
    """
    test_ansible_dir = os.path.join(
        os.path.dirname(os.path.abspath(__file__)), 'data/ansible/')
    defs.AnsiblePaths.initialise(test_ansible_dir)


@pytest.fixture
def data_directory():
    """
    Fixture to return the path to the data directory that contains data
    used by the tests.

    :return: The full path to the data directory
    :rtype: str
    """
    root_directory = os.path.dirname(os.path.abspath(__file__))
    data_directory = os.path.join(root_directory, 'data')
    return data_directory
