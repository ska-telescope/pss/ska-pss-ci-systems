import os
import pytest
import subprocess
import tempfile

import iac_deployer.colours as colours
import iac_deployer.definitions as defs
import iac_deployer.deploy as deploy
import iac_deployer.status as status


class TestDeploy:
    """
    Class for testing the deploy module.
    """

    @pytest.fixture(scope='session', autouse=True)
    def local_status_repository(self):
        """
        Creates a temporary directory and runs git init in it to turn
        it into a local git repository that can be used in place of the
        actual status repository.

        :yield: The path to the temporary directory, that is the local
            status repository
        :rtype: str
        """
        original_status_repository = status.STATUS_REPO_DIR
        with tempfile.TemporaryDirectory() as status_repo_directory:
            status.STATUS_REPO_DIR = status_repo_directory
            try:
                os.chdir(status_repo_directory)
                subprocess.run(['git', 'init'], check=True)
            finally:
                os.chdir(defs.REPOSITORY_ROOT_DIR)
            yield status_repo_directory
        # Restore original status repository path
        status.STATUS_REPO_DIR = original_status_repository

    def test_verbose_quiet_mutual_exclusion_error(self, capsys):
        """
        Tests that the deployment fails if verbose and quiet are both
        set to True.

        :param capsys: pytest capsys fixture for capturing stdout /
            stderr
        """
        assert not deploy.deploy([], verbose=True, quiet=True,
                                 is_git_repository_override=False)
        captured = capsys.readouterr()
        assert (
            f"{colours.FAIL}--verbose and --quiet are mutually exclusive"
            f"{colours.CLEAR}"
        ) in captured.out.splitlines()

    def test_check_git_issues(self, capsys):
        """
        Tests that the deployment fails if there are unpushed commits
        to the remote or the repository is dirty (i.e. there are local
        modifications).

        :param capsys: pytest capsys fixture for capturing stdout /
            stderr
        """
        is_unpushed_output = (
            """
            commit 1234567891011121314151617181920212223242
            Author: A User <123456-user@users.noreply.gitlab.com>
            Date:   Mon Jan 1 00:00:00 0000 +0000

                A commit title
                - Commit detail
            """
        )
        is_clean_output = ' M python/src/iac_deployer/definitions.py'
        assert not deploy.deploy(
            [],
            is_clean_testing_output=is_clean_output,
            is_unpushed_testing_output=is_unpushed_output,
            is_git_repository_override=True
        )
        captured = capsys.readouterr()
        assert (
            f"{colours.FAIL}"
            'Git checks failed. Please check the above error message(s) '
            'and try again.'
            f"{colours.CLEAR}"
        ) in captured.out
