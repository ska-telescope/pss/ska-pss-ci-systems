import os
import pytest

import iac_deployer.colours as colours
import iac_deployer.definitions as defs
import iac_deployer.machine_test as machine_test


class TestMachineTest:
    """
    Class for testing the machine_test module.
    """

    @pytest.fixture
    def machine_names(self):
        """
        Returns a list of available machine names. The first one has
        valid syntax, the second does not. The third has valid syntax
        but is missing the crucial os variable.

        :return: The list of the machine playbook names from the test
            ansible/hosts directory
        :rtype: list
        """
        return ['test_machine_1', 'test_machine_2', 'test_machine_3']

    @pytest.fixture
    def inventory_missing_machine_1(self):
        """
        Returns the path to the inventory file that is missing
        test_machine_1. Sets the inventory_file definition to this file
        temporarily, before setting it back to what it was before.

        :yield: The path to the inventory file that is missing
            test_machine_1
        :rtype: str
        """
        original_inventory_file = defs.AnsiblePaths.inventory_file
        inventory_missing_machine = 'production_missing_machine_1'
        path, _ = os.path.split(defs.AnsiblePaths.inventory_file)
        defs.AnsiblePaths.inventory_file = os.path.join(
            path, inventory_missing_machine)
        yield defs.AnsiblePaths.inventory_file
        defs.AnsiblePaths.inventory_file = original_inventory_file

    def test_print_machine_tests(self, machine_names, capsys):
        """
        Tests the print_machine_tests() function.

        :param machine_names: fixture providing a list of the available
            test machine names
        :type machine_names: list
        :param capsys: pytest capsys fixture for capturing stdout /
            stderr
        """
        machine_test.print_machine_tests()
        captured = capsys.readouterr()
        assert set(captured.out.splitlines()) == set(machine_names)

    def test_invalid_machine_name(self, capsys):
        """
        Test to ensure that the MachineTest fails when providing it
        with an invalid machine name.

        :param capsys: pytest capsys fixture for capturing stdout /
            stderr
        """
        invalid_machine_name = 'invalid_machine_name'
        test = machine_test.MachineTest(invalid_machine_name)
        assert not test.run(docker_install_override=True,
                            docker_permission_override=True)
        captured = capsys.readouterr()
        assert captured.out.splitlines() == [
            f"[STAGE] Initialising {invalid_machine_name} machine test",
            f"{colours.FAIL}Could not find any of {invalid_machine_name}.yml,"
            f" {invalid_machine_name}.yaml in "
            f"{defs.AnsiblePaths.host_playbooks_directory}{colours.CLEAR}",
            f"Please create a playbook for this machine using the provided "
            f"template in {defs.AnsiblePaths.host_playbooks_directory}",
            f"{colours.FAIL}Test FAILED!{colours.CLEAR}"
        ]

    def test_invalid_inventory(self, machine_names, capsys):
        """
        Test to ensure that the MachineTest fails when providing it
        with an invalid inventory path.

        :param machine_names: fixture providing a list of the available
            test machine names
        :type machine_names: list
        :param capsys: pytest capsys fixture for capturing stdout /
            stderr
        """
        original_inventory_file = defs.AnsiblePaths.inventory_file
        try:
            invalid_inventory_name = 'this_is_not_a_valid_inventory_filename'
            path, _ = os.path.split(defs.AnsiblePaths.inventory_file)
            defs.AnsiblePaths.inventory_file = os.path.join(
                path, invalid_inventory_name)
            test = machine_test.MachineTest(machine_names[0])
            assert not test.run(docker_install_override=True,
                                docker_permission_override=True)
            captured = capsys.readouterr()
            assert captured.out.splitlines() == [
                f"[STAGE] Initialising {machine_names[0]} machine test",
                f"{colours.FAIL}No inventory file found at "
                f"{str(defs.AnsiblePaths.inventory_file)}. Please create one "
                f"and try again.{colours.CLEAR}",
                f"{colours.FAIL}Test FAILED!{colours.CLEAR}"
            ]
        finally:
            # Reset inventory file back to what it was for subsequent
            # tests
            defs.AnsiblePaths.inventory_file = original_inventory_file

    def test_invalid_playbook_syntax(self, machine_names, capsys):
        """
        Test to ensure that the MachineTest fails when providing it
        with a machine playbook with invalid syntax.

        :param machine_names: fixture providing a list of the available
            machine names
        :type machine_names: list
        :param capsys: pytest capsys fixture for capturing stdout /
            stderr
        """
        test = machine_test.MachineTest(machine_names[1])
        assert not test.run(docker_install_override=True,
                            docker_permission_override=True)
        captured = capsys.readouterr()
        captured_lines = captured.out.splitlines()
        error_msgs = [
            'Please check the syntax of the machine and group playbooks and '
            'try again. Full error details:',
            f"Syntax check for '{machine_names[1]}.yml' playbook failed! "
            'Please check it and try again.',
            f"{colours.FAIL}Test FAILED!{colours.CLEAR}"
        ]
        # Assert that captured_lines ends with the error_msgs
        assert (captured_lines[len(captured_lines) - len(error_msgs):] ==
                error_msgs)

    def test_no_os_variable(self, machine_names, capsys):
        """
        Test to ensure that the MachineTest fails when providing it
        with a machine playbook that doesn't contain the os variable.

        :param machine_names: fixture providing a list of the available
            machine names
        :type machine_names: list
        :param capsys: pytest capsys fixture for capturing stdout /
            stderr
        """
        test = machine_test.MachineTest(machine_names[2])
        assert not test.run(docker_install_override=True,
                            docker_permission_override=True)
        captured = capsys.readouterr()
        captured_lines = captured.out.splitlines()
        error_msgs = [
            f"{colours.FAIL}Could not find os variable from "
            f"{machine_names[2]} playbook. Please ensure the playbook uses "
            'the same format as the template file and try again.'
            f"{colours.CLEAR}",
            f"{colours.FAIL}Test FAILED!{colours.CLEAR}"
        ]
        # Assert that captured_lines ends with the error_msgs
        assert (captured_lines[len(captured_lines) - len(error_msgs):] ==
                error_msgs)

    def test_inventory_missing_machine(self, inventory_missing_machine_1,
                                       machine_names, capsys):
        """
        Test to ensure that the MachineTest fails when providing it
        with an inventory that is missing the machine that it is
        running the test for

        :param inventory_missing_machine_1: fixture providing the path
            to the inventory that is missing test_machine_1
        :type inventory_missing_machine_1: str
        :param machine_names: fixture providing a list of the available
            machine names
        :type machine_names: list
        :param capsys: pytest capsys fixture for capturing stdout /
            stderr
        """
        test = machine_test.MachineTest(machine_names[0])
        assert not test.run(docker_install_override=True,
                            docker_permission_override=True)
        captured = capsys.readouterr()
        captured_lines = captured.out.splitlines()
        error_msgs = [
            f"{colours.FAIL}[ERROR] Could not find an entry for "
            f"{machine_names[0]} in the inventory file. Please check the "
            f"inventory ({inventory_missing_machine_1}) and try again."
            f"{colours.CLEAR}",
            f"{colours.FAIL}Test FAILED!{colours.CLEAR}"
        ]
        # Assert that captured_lines ends with the error_msgs
        assert (captured_lines[len(captured_lines) - len(error_msgs):] ==
                error_msgs)

    def test_successful_test(self, machine_names, capsys):
        """
        Test to ensure that the MachineTest passes when there are no
        issues

        :param machine_names: fixture providing a list of the available
            machine names
        :type machine_names: list
        :param capsys: pytest capsys fixture for capturing stdout /
            stderr
        """
        test = machine_test.MachineTest(machine_names[0], no_docker=True)
        assert test.run(docker_install_override=True,
                        docker_permission_override=True)
        captured = capsys.readouterr()
        captured_lines = captured.out.splitlines()
        assert (captured_lines[-1] == f"{colours.SUCCESS}Test SUCCESS"
                                      f"{colours.CLEAR}")
