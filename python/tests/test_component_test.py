import json
import os
import pytest
import shutil

import iac_deployer.colours as colours
import iac_deployer.component_test as component_test
import iac_deployer.definitions as defs


class TestComponentTest:

    @pytest.fixture
    def component_tests(self):
        """
        Returns the list of available components that can be tested.

        :return: The list of names of the components
        :rtype: list of strs
        """
        return [f"test-component-{i}" for i in range(1, 6)]

    @pytest.fixture
    def test_docker_diff(self, data_directory):
        """
        Returns the contents of the docker diff sample file within
        a dictionary.

        :param data_directory: The data directory fixture (gives the
            path to it)
        :type data_directory: str
        :return: The file's contents
        :rtype: list of dicts
        """
        filename = 'test_docker_diff.json'
        path = os.path.join(data_directory, filename)
        with open(path, 'r') as file_stream:
            result = json.load(file_stream)
        return result

    @pytest.fixture
    def os_files_dir(self, data_directory):
        """
        Provides the path to the os_files directory within tests/data.
        The os_files directory represents a valid directory structure
        for storing OS Dockerfiles.
        Also sets AnsiblePaths.os_dockerfiles_directory to the path to
        the directory, before resetting it back to was it was
        originally, after the fixture has been used.

        :param data_directory: The data directory fixture (gives the
            path to it)
        :type data_directory: str
        :yield: The full path to the os_files directory
        :rtype: str
        """
        backup = defs.AnsiblePaths.os_dockerfiles_directory
        path = os.path.join(data_directory, 'os_files')
        defs.AnsiblePaths.os_dockerfiles_directory = path
        yield path
        defs.AnsiblePaths.os_dockerfiles_directory = backup

    @pytest.fixture
    def non_os_files_dir(self, data_directory):
        """
        Provides the path to the non_os_files directory within
        tests/data.
        The non_os_files directory represents an invalid directory
        structure for storing OS Dockerfiles.
        Also sets AnsiblePaths.os_dockerfiles_directory to the path to
        the directory, before resetting it back to was it was
        originally, after the fixture has been used.

        :param data_directory: The data directory fixture (gives the
            path to it)
        :type data_directory: str
        :yield: The full path to the non_os_files directory
        :rtype: str
        """
        backup = defs.AnsiblePaths.os_dockerfiles_directory
        path = os.path.join(data_directory, 'non_os_files')
        defs.AnsiblePaths.os_dockerfiles_directory = path
        yield path
        defs.AnsiblePaths.os_dockerfiles_directory = backup

    @pytest.fixture
    def role_without_component_test(self):
        """
        Creates a role called "role_without_component_test" and yields
        its name. After usage, the role files are deleted.

        :yield: The name of the role (role_without_component_test)
        :rtype: str
        """
        role_name = 'role_without_component_test'
        role_path = os.path.join(defs.AnsiblePaths.roles_directory, role_name)
        tasks_path = os.path.join(role_path, 'tasks')
        os.makedirs(tasks_path)
        main_yaml_path = os.path.join(tasks_path, 'main.yml')
        with open(main_yaml_path, 'w') as file_stream:
            file_stream.write('# Empty file')
        yield role_name
        shutil.rmtree(role_path)

    def test_check_component_tests_passes(self):
        """
        Test to ensure that check_component_tests() passes when all
        components have corresponding component tests.
        """
        component_test.check_component_tests()

    def test_check_component_tests_fails(self, role_without_component_test):
        """
        Test to ensure that check_component_tests() fails when there is
        at least one role that lacks a corresponding component test.

        :param role_without_component_test: The fixture for the role
            that lacks a corresponding component test
        :type role_without_component_test: str
        """
        # This should fail because we're now making use of
        # role_without_component_test
        with pytest.raises(component_test.MissingComponentTestsError):
            component_test.check_component_tests()

    def test_print_component_tests(self, component_tests, capsys):
        """
        Test for the print_component_tests() function.

        :param component_tests: The fixture for the list of the names
            of the available components that can be tested
        :type component_tests: list of strs
        :param capsys: pytest capsys fixture for capturing stdout /
            stderr
        """
        component_test.print_component_tests()
        captured = capsys.readouterr()
        assert captured.out.splitlines().sort() == component_tests.sort()

    def test_run(self, component_tests, capsys):
        """
        Tests creating and running ComponentTests.

        :param component_tests_dir: The component tests directory
            fixture (the full path to it)
        :type component_tests_dir: str
        :param capsys: pytest capsys fixture for capturing stdout /
            stderr
        """
        # Test valid component test
        test = component_test.ComponentTest(
            component_tests[0],
            verbose=False,
            test_docker_diff=[])
        assert test.run(docker_install_override=True,
                        docker_permission_override=True)
        captured = capsys.readouterr()
        captured_lines = captured.out.splitlines()
        assert (f"[STAGE] Run {component_tests[0]} setup playbook" in
                captured_lines)
        assert (f"[STAGE] Run {component_tests[0]} teardown playbook" in
                captured_lines)
        assert (f"{colours.SUCCESS}Test SUCCESS{colours.CLEAR}" in
                captured_lines)
        assert (f"{colours.FAIL}Test FAILED!{colours.CLEAR}" not in
                captured_lines)

        # Test invalid component test
        test = component_test.ComponentTest(
            component_tests[1],
            verbose=False,
            test_docker_diff=[])
        assert not test.run(docker_install_override=True,
                            docker_permission_override=True)
        captured = capsys.readouterr()
        captured_lines = captured.out.splitlines()
        assert (f"{colours.SUCCESS}Test SUCCESS{colours.CLEAR}" not in
                captured_lines)
        assert f"{colours.FAIL}Test FAILED!{colours.CLEAR}" in captured_lines

        # Test invalid component test directory
        invalid_dir = 'this is not a directory'
        test = component_test.ComponentTest(
            invalid_dir,
            verbose=False,
            test_docker_diff=[])
        assert not test.run(docker_install_override=True,
                            docker_permission_override=True)
        captured = capsys.readouterr()
        captured_lines = captured.out.splitlines()
        assert (f"{colours.FAIL}Couldn't find {invalid_dir} directory in "
                f"{defs.AnsiblePaths.component_tests_directory}"
                f"{colours.CLEAR}" in captured_lines)
        assert ('Please create a directory for this component and try again.'
                in captured_lines)

    def test_os_list(self, component_tests, os_files_dir, capsys):
        """
        Tests to ensure that the OS list functionality works correctly
        when using the valid OS files directory.

        :param component_tests: The fixture for the list of the names
            of the available components that can be tested
        :type component_tests: list of strs
        :param os_files_dir: The valid OS files directory fixture
            (provides the full path to it)
        :type os_files_dir: str
        :param capsys: pytest capsys fixture for capturing stdout /
            stderr
        """
        # Name of the deploy in the test-component-1 directory
        deploy_playbook = 'deploy.yml'

        # Test providing an OS list
        os_name = 'ubuntu-20.04'
        test = component_test.ComponentTest(
            component_tests[0], os_list=[os_name], verbose=False,
            test_docker_diff=[])
        assert test.run(docker_install_override=True,
                        docker_permission_override=True)
        captured = capsys.readouterr()
        captured_lines = captured.out.splitlines()
        print(captured_lines)
        assert (f"[STAGE] Running {component_tests[0]}/{deploy_playbook} "
                f"playbook with {os_name} as the base" in captured_lines)

        # Test not providing an OS list
        os_list = ['ubuntu-20.04', 'centos-7']
        test = component_test.ComponentTest(
            component_tests[0], verbose=False,
            test_docker_diff=[])
        assert test.run(docker_install_override=True,
                        docker_permission_override=True)
        captured = capsys.readouterr()
        captured_lines = captured.out.splitlines()
        for os_name in os_list:
            assert (f"[STAGE] Running {component_tests[0]}/{deploy_playbook} "
                    f"playbook with {os_name} as the base" in captured_lines)

        # Test providing an invalid OS name
        os_list = ['ubuntu-20.04', 'centos-7', 'invalid-os-name']
        test = component_test.ComponentTest(
            component_tests[0], os_list=os_list, verbose=False,
            test_docker_diff=[])
        assert not test.run(docker_install_override=True,
                            docker_permission_override=True)
        captured = capsys.readouterr()
        captured_lines = captured.out.splitlines()
        assert (f"{colours.FAIL}[ERROR] Could not find {os_list[-1]} "
                f"directory in {os_files_dir}{colours.CLEAR}" in
                captured_lines)

        # Test using the no-dockerfile folder
        os_name = 'no-dockerfile'
        test = component_test.ComponentTest(
            component_tests[0], os_list=[os_name], verbose=False,
            test_docker_diff=[])
        assert not test.run(docker_install_override=True,
                            docker_permission_override=True)
        captured = capsys.readouterr()
        captured_lines = captured.out.splitlines()
        assert (f"{colours.FAIL}[ERROR] {os.path.join(os_files_dir, os_name)}"
                ' does not contain a Dockerfile! Please create one and try '
                f"again.{colours.CLEAR}" in captured_lines)

    def test_non_os_list(self, component_tests, non_os_files_dir, capsys):
        """
        Tests to ensure that the OS list functionality works correctly
        when using the invalid OS files directory.

        :param component_tests: The fixture for the list of the names
            of the available components that can be tested
        :type component_tests: list of strs
        :param non_os_files_dir: The invalid OS files directory fixture
            (provides the full path to it)
        :type non_os_files_dir: str
        :param capsys: pytest capsys fixture for capturing stdout /
            stderr
        """
        # Test using the non_os_files folder
        test = component_test.ComponentTest(
            component_tests[0], verbose=False,
            test_docker_diff=[])
        assert not test.run(docker_install_override=True,
                            docker_permission_override=True)
        captured = capsys.readouterr()
        captured_lines = captured.out.splitlines()
        assert (f"{colours.FAIL}[ERROR] Could not find any operating system "
                f"directories with Dockerfiles in {non_os_files_dir}. Please "
                f"create at least one directory named after an OS with a "
                f"Dockerfile within it and try again.{colours.CLEAR}" in
                captured_lines)

    def test_no_deploy_playbook(self, component_tests, capsys):
        """
        Tests to ensure that the test fails if there is no deploy
        playbook for the component being tested.

        :param component_tests: The fixture for the list of the names
            of the available components that can be tested
        :type component_tests: list of strs
        :param capsys: pytest capsys fixture for capturing stdout /
            stderr
        """
        test = component_test.ComponentTest(
            component_tests[2],
            test_docker_diff=[])
        assert not test.run(docker_install_override=True,
                            docker_permission_override=True)
        captured = capsys.readouterr()
        captured_lines = captured.out.splitlines()
        err_msg = (f"Please create the deploy playbook file for "
                   f"the {component_tests[2]} component and try again.")
        assert (err_msg in captured_lines)
        assert (f"{colours.SUCCESS}Test SUCCESS{colours.CLEAR}" not in
                captured_lines)
        assert (f"{colours.FAIL}Test FAILED!{colours.CLEAR}" in
                captured_lines)

    def test_no_ignore_list(self, component_tests, test_docker_diff, capsys):
        """
        Tests to ensure that the test fails if file changes occur which
        are not on the ignore list.

        :param component_tests: The fixture for the list of the names
            of the available components that can be tested
        :type component_tests: list of strs
        :param test_docker_diff: The fixture for the sample docker diff
            output to be used in place of the actual docker command
        :type test_docker_diff: list of dicts
        :param capsys: pytest capsys fixture for capturing stdout /
            stderr
        """
        test = component_test.ComponentTest(
            component_tests[0],
            test_docker_diff=test_docker_diff)
        assert not test.run(docker_install_override=True,
                            docker_permission_override=True)
        captured = capsys.readouterr()
        captured_str = captured.out
        print(captured_str)
        captured_lines = captured.out.splitlines()
        created_files_str = (
            '[Files that were created but weren\'t deleted]\n'
            '/run/docker.sock\n'
            '/root/.ansible\n'
            '/root/.ansible/tmp\n'
            '/usr/lib/python3/dist-packages/__pycache__\n'
            '/usr/lib/python3/dist-packages/__pycache__/'
            'lsb_release.cpython-38.pyc\n'
        )
        assert created_files_str in captured_str
        deleted_files_str = (
            '[Files that were deleted]\n'
            '/some_random_file.txt\n'
        )
        assert deleted_files_str in captured_str
        err_msg = (
            f"{colours.FAIL}Changes for files that weren't on "
            'the ignore list occurred. Please see the output '
            'above and either modify the component role to deal'
            ' with these files or add them to the ignore list.'
            f"{colours.CLEAR}"
        )
        assert err_msg in captured_lines
        assert (f"{colours.SUCCESS}Test SUCCESS{colours.CLEAR}" not in
                captured_lines)
        assert (f"{colours.FAIL}Test FAILED!{colours.CLEAR}" in
                captured_lines)

    def test_ignore_list(self, component_tests, test_docker_diff, capsys):
        """
        Tests to ensure that the test passes if file changes occur which
        are on the ignore list.

        :param component_tests: The fixture for the list of the names
            of the available components that can be tested
        :type component_tests: list of strs
        :param test_docker_diff: The fixture for the sample docker diff
            output to be used in place of the actual docker command
        :type test_docker_diff: list of dicts
        :param capsys: pytest capsys fixture for capturing stdout /
            stderr
        """
        test = component_test.ComponentTest(
            component_tests[3],
            test_docker_diff=test_docker_diff)
        assert test.run(docker_install_override=True,
                        docker_permission_override=True)
        captured = capsys.readouterr()
        captured_lines = captured.out.splitlines()
        print(captured_lines)
        created_files_str = '[Files that were created but weren\'t deleted]'
        assert created_files_str not in captured_lines
        deleted_files_str = '[Files that were deleted]'
        assert deleted_files_str not in captured_lines
        err_msg = (
            f"{colours.FAIL}Changes for files that weren't on "
            'the ignore list occurred. Please see the output '
            'above and either modify the component role to deal'
            ' with these files or add them to the ignore list.'
            f"{colours.CLEAR}"
        )
        assert err_msg not in captured_lines
        assert (f"[INFO] Found role dependencies for {component_tests[3]}: "
                f"{component_tests[1]}" in captured_lines)
        assert (f"{colours.SUCCESS}Test SUCCESS{colours.CLEAR}" in
                captured_lines)
        assert (f"{colours.FAIL}Test FAILED!{colours.CLEAR}" not in
                captured_lines)

    def test_circular_dependency(self, component_tests, test_docker_diff,
                                 capsys):
        """
        Tests to ensure that the test fails if the component being
        tested has a circular dependency. Also tests that any invalid
        dependency syntax in the role meta file is reported but
        ultimately ignored.

        :param component_tests: The fixture for the list of the names
            of the available components that can be tested
        :type component_tests: list of strs
        :param test_docker_diff: The fixture for the sample docker diff
            output to be used in place of the actual docker command
        :type test_docker_diff: list of dicts
        :param capsys: pytest capsys fixture for capturing stdout /
            stderr
        """
        test = component_test.ComponentTest(
            component_tests[4],
            test_docker_diff=test_docker_diff)
        assert not test.run(docker_install_override=True,
                            docker_permission_override=True)
        captured = capsys.readouterr()
        captured_lines = captured.out.splitlines()
        print(captured_lines)
        err_msg = ('You appear to have circular dependencies '
                   'present in your role definitions. Please '
                   'check the dependencies carefully, fix the '
                   'problem, and try again.')
        assert err_msg in captured_lines
        dependency = {"invalid_key": "test"}
        err_msg = (f"{colours.WARNING}[WARNING] Couldn't parse object "
                   f"in role list: {str(dependency)}. You may wish to "
                   f"check this.{colours.CLEAR}")
        assert err_msg in captured_lines
        assert (f"{colours.SUCCESS}Test SUCCESS{colours.CLEAR}" not in
                captured_lines)
        assert (f"{colours.FAIL}Test FAILED!{colours.CLEAR}" in
                captured_lines)
