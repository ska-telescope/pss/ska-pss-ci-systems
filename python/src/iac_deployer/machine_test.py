import os
import subprocess
import yaml

from . import colours
from . import definitions as defs
from . import machine
from . import test
from . import utils


def get_machine_tests():
    """
    Retrieves the names of the machine playbooks that are present in
    ansible/hosts/. These represent configurations that can be tested.
    The playbook filename extensions won't be included.

    :return: A list of the machine names
    :rtype: list of strs
    """
    result = []
    for yaml_file in utils.get_directory_yaml_files(
            defs.AnsiblePaths.host_playbooks_directory):
        result.append(os.path.splitext(yaml_file)[0])
    return result


def print_machine_tests():
    """
    Prints out the names of the machine playbooks that are present in
    ansible/hosts/. These represent configurations that can be tested.
    The playbook filename extensions won't be printed out.
    """
    for machine_name in get_machine_tests():
        print(machine_name)


class MachineTest(test.Test):
    """
    Class representing a test of a machine's Ansible configuration
    """

    def __init__(self, machine_name, verbose=False,
                 force_docker_install=False, no_docker_sudo_prompt=False,
                 no_docker=False, pause_before_teardown=False):
        """
        Constructor for the test of the machine's configuration.

        :param machine_name: The name of the machine to be tested
        :type machine_name: str
        :param verbose: Increase output verbosity, defaults to False
        :type verbose: bool, optional
        :param force_docker_install: If True, don't prompt the user if
            docker needs installing - just install it. Defaults to
            False
        :type force_docker_install: bool, optional
        :param no_docker_sudo_prompt: If True, don't prompt the user
            for a sudo password when running the Docker install
            playbook. Useful for unit testing. Defaults to False
        :type no_docker_sudo_prompt: bool, optional
        :param no_docker: For unit testing; if True, don't attempt to
            connect to a local Docker container, just use localhost
            instead for deploying the machine's config, defaults to
            False
        :type no_docker: bool, optional
        :param pause_before_teardown: If True, wait for user input
            before tearing down the test resources. Useful for
            debugging. Defaults to False
        :type pause_before_teardown: bool, optional
        """
        test.Test.__init__(
            self, verbose=verbose, force_docker_install=force_docker_install,
            no_docker_sudo_prompt=no_docker_sudo_prompt,
            pause_before_teardown=pause_before_teardown)

        # Basic options
        self.machine_name = machine_name
        # Will store what operating system that the machine should have
        self.machine_os = None

        # Stores the ideal name of the generated playbook file
        self.ideal_playbook_name = 'site.yml'
        # Will store the actual path (which may differ if site.yml
        # already exists)
        self.actual_playbook_path = None
        # Will store the paths to the generated inventory files; the
        # first for deploy mode and the second for rollback mode
        self.generated_inventories = []
        # Stores whether the Docker container for the test has been
        # created yet
        self.container_created = False
        # Stores the name of the Docker container that will be used for
        # the test
        self.container_name = self.machine_name + '-' + self.uuid
        # Stores the path to the machine test teardown playbook (it
        # will remain None if there is no teardown playbook)
        self.machine_test_teardown_path = None

        # For unit testing
        self.no_docker = no_docker

    def _teardown_test(self):
        """
        Remove the generated test playbook and inventory file if they
        exist and run the docker-destroy playbook to remove the test
        Docker container.

        :raises err: CalledProcessError if an error occurred when
            running the docker-destroy playbook with Ansible
        """
        # Delete the generated site.yml and inventory file
        if self.actual_playbook_path and os.path.isfile(
                self.actual_playbook_path):
            os.remove(self.actual_playbook_path)
        for inventory in self.generated_inventories:
            if os.path.isfile(inventory):
                os.remove(inventory)

        try:
            if self.container_created:
                # Tear down test Docker container
                try:
                    print('\n[STAGE] Tear down Docker container')
                    utils.run_playbook(
                        defs.AnsiblePaths.docker_destroy_playbook,
                        extra_vars=f"container_name={self.container_name} "
                                   f"{defs.TEST_UUID_KEY}={self.uuid} "
                                   f"{defs.TEST_DOCKER_NETWORK_KEY}="
                                   f"{self.docker_network_name}",
                        verbose=self.verbose,
                        print_command_output=self.verbose
                    )
                except subprocess.CalledProcessError as err:
                    print('Error when tearing down container!')
                    raise err
        finally:
            if self.machine_test_teardown_path:
                print(f"[STAGE] Run {self.machine_name} teardown playbook")
                try:
                    utils.run_playbook(
                        self.machine_test_teardown_path, verbose=self.verbose,
                        extra_vars=f"{defs.TEST_UUID_KEY}={self.uuid} "
                                   f"{defs.TEST_DOCKER_NETWORK_KEY}="
                                   f"{self.docker_network_name}")
                except subprocess.CalledProcessError as err:
                    print(f"Error when running the {self.machine_name} "
                          'teardown playbook ('
                          f"{self.machine_test_teardown_path}), please check "
                          'it and try again.')
                    raise err

    def _execute_test(self):
        """
        Runs the actual test steps for testing the machine's config.

        Performs various checks first e.g. does the machine playbook
        exist, does the inventory file exist, does the machine playbook
        specify the os variable, etc.

        Then, generates a new playbook from the machine playbook plus
        all the group playbooks that the machine is a part of,
        generates a new inventory file that changes the machine's
        connection method to Docker, creates a Docker container named
        after the machine based on the machine's OS, runs the generated
        playbook on that container with the generated inventory file,
        firstly in dry run mode, then in deploy and rollback mode. Uses
        docker to check for any file differences between dry run and
        deploy / rollback. If no errors occurred, the test passes,
        otherwise it fails.

        :return: The result of the test; True for success, False for
            failure
        :rtype: bool
        """
        print(f"[STAGE] Initialising {self.machine_name} machine test")

        # Ensure machine playbook exists
        try:
            machine_playbook_file = utils.get_directory_yaml_file(
                defs.AnsiblePaths.host_playbooks_directory, self.machine_name)
        except FileNotFoundError as err:
            print(f"{colours.FAIL}{err}{colours.CLEAR}")
            print('Please create a playbook for this machine using the '
                  'provided template in '
                  f"{defs.AnsiblePaths.host_playbooks_directory}")
            return False

        # Ensure inventory file exists
        if not os.path.isfile(defs.AnsiblePaths.inventory_file):
            print(f"{colours.FAIL}No inventory file found at "
                  f"{str(defs.AnsiblePaths.inventory_file)}. Please create "
                  f"one and try again.{colours.CLEAR}")
            return False

        try:
            # Concatenate all host and group playbooks together into
            # one playbook
            self.actual_playbook_path = utils.concatenate_playbooks(
                self.machine_name,
                final_playbook_name=self.ideal_playbook_name,
                verbose=self.verbose)
        except SyntaxError as err:
            print('Please check the syntax of the machine and group '
                  'playbooks and try again. Full error details:')
            print(err)
            return False

        try:
            self.machine_os, machine_components = (
                machine.parse_machine_playbook(machine_playbook_file,
                                               self.machine_name)
            )
        except yaml.parser.ParserError as err:
            print(f"{colours.FAIL}Invalid YAML! Please check "
                  f"{machine_playbook_file} and try again. Error details:"
                  f"{colours.CLEAR}")
            print(err)
            return False
        # Print an error message if the os variable couldn't be found
        if self.machine_os is None:
            print(
                f"{colours.FAIL}Could not find os variable from "
                f"{self.machine_name} playbook. Please ensure the playbook "
                f"uses the same format as the template file and try again."
                f"{colours.CLEAR}"
            )
            return False

        # Disallowed and new variables that will be used for the
        # machine's entry in the inventory file
        disallowed_vars = [
            'ansible_host=',
            'ansible_user=',
            'ansible_connection=',
            'ansible_become=',
            'ansible_docker_extra_args=',
            'deploy_mode='
        ]
        new_vars = ['deploy_mode=deploy']
        if self.no_docker:
            # For unit testing; don't use docker and
            # just connect locally if self.no_docker is
            # True
            new_vars += ['ansible_host=127.0.0.1', 'ansible_connection=local']
        else:
            # Add ansible_connection=docker to the machine's line in
            # the inventory to ensure Ansible connects to the machine
            # container locally via Docker
            # Also set ansible_host to the container name so it
            # connects to the right container
            new_vars += ['ansible_host=' + self.container_name,
                         'ansible_connection=docker']

        # Generate the inventories to use for testing with, passing the
        # disallowed and new variable values. The first inventory is
        # for deploy mode, the second for rollback mode.
        for mode in ['deploy_mode=deploy', 'deploy_mode=rollback']:
            new_vars[0] = mode
            try:
                self.generated_inventories.append(
                    utils.gen_inventory_with_altered_vars(
                        [self.machine_name], disallowed_vars, new_vars)
                )
            except utils.InventoryMissingHostError as err:
                print(err)
                return False

        # Will hold the path to the file containing any extra vars
        # needed for testing
        machine_test_vars_path = None

        # Check to see if a test directory for the machine exists
        machine_test_directory = os.path.join(
            defs.AnsiblePaths.machine_tests_directory, self.machine_name)
        if (os.path.exists(machine_test_directory) and
                os.path.isdir(machine_test_directory)):
            try:
                machine_test_vars_path = utils.get_directory_yaml_file(
                    machine_test_directory,
                    defs.AnsiblePaths.machine_test_vars_file
                )
            except FileNotFoundError:
                pass

            try:
                machine_test_setup_path = utils.get_directory_yaml_file(
                    machine_test_directory,
                    defs.AnsiblePaths.machine_test_setup_file
                )
            except FileNotFoundError:
                # No setup playbook exists, so don't run it
                machine_test_setup_path = None

            try:
                teardown_file_path = utils.get_directory_yaml_file(
                    machine_test_directory,
                    defs.AnsiblePaths.machine_test_teardown_file
                )
            except FileNotFoundError:
                # Just ignore and move on if the teardown file doesn't
                # exist
                pass
            else:
                self.machine_test_teardown_path = teardown_file_path

            if machine_test_setup_path:
                print(f"[STAGE] Run {self.machine_name} setup playbook")
                try:
                    utils.run_playbook(
                        machine_test_setup_path, verbose=self.verbose,
                        extra_vars=f"{defs.TEST_UUID_KEY}={self.uuid} "
                                   f"{defs.TEST_DOCKER_NETWORK_KEY}="
                                   f"{self.docker_network_name}")
                except subprocess.CalledProcessError:
                    print(f"Error when running the {self.machine_name} setup "
                          f"playbook ({machine_test_setup_path}), please "
                          'check it and try again.')
                    return False

        # Create test docker container
        print(f"[STAGE] Create Docker container with base OS "
              f"{self.machine_os} to test deployment")
        try:
            utils.run_playbook(
                defs.AnsiblePaths.docker_create_playbook,
                extra_vars=f"container_name={self.container_name} "
                           f"os={self.machine_os} "
                           f"{defs.TEST_UUID_KEY}={self.uuid} "
                           f"{defs.TEST_DOCKER_NETWORK_KEY}="
                           f"{self.docker_network_name}",
                verbose=self.verbose, print_command_output=self.verbose
            )
            self.container_created = True
        except subprocess.CalledProcessError:
            print('[ERROR] Couldn\'t create test Docker container!')
            return False

        if machine_test_vars_path:
            machine_test_vars_path = '@' + machine_test_vars_path

        extra_vars = []
        if machine_test_vars_path:
            extra_vars.append(machine_test_vars_path)

        # Add any found vars.y(a)ml files from the machine's components
        # directories to be used as extra vars for the Ansible commands
        for machine_component in machine_components:
            try:
                component_var_path = utils.get_directory_yaml_file(
                    os.path.join(defs.AnsiblePaths.component_tests_directory,
                                 machine_component),
                    defs.AnsiblePaths.component_test_vars_file)
                extra_vars.append('@' + component_var_path)
                print(
                    f"[INFO] Using {os.path.basename(component_var_path)} "
                    f"from {machine_component} test directory"
                )
            except FileNotFoundError:
                pass

        # Run playbook on created docker container (in check mode
        # first)
        print(f"[STAGE] Test {self.machine_name} playbook in dry-run mode")
        try:
            utils.run_playbook(
                self.actual_playbook_path,
                inventory_path=self.generated_inventories[0],
                limit=self.machine_name,
                user=defs.CONTAINER_TEST_USER,
                check=True,
                diff=True,
                extra_vars=extra_vars + [f"{defs.TEST_UUID_KEY}={self.uuid}",
                                         f"{defs.TEST_DOCKER_NETWORK_KEY}="
                                         f"{self.docker_network_name}"],
                force_venv_python=False,
                verbose=self.verbose
            )
        except subprocess.CalledProcessError:
            return False

        # Run playbook on created docker container (in deploy mode)
        print(f"\n[STAGE] Test {self.machine_name} playbook in deploy mode")
        try:
            utils.run_playbook(
                self.actual_playbook_path,
                inventory_path=self.generated_inventories[0],
                limit=self.machine_name,
                user=defs.CONTAINER_TEST_USER,
                extra_vars=extra_vars + [f"{defs.TEST_UUID_KEY}={self.uuid}",
                                         f"{defs.TEST_DOCKER_NETWORK_KEY}="
                                         f"{self.docker_network_name}"],
                force_venv_python=False,
                verbose=self.verbose
            )
        except subprocess.CalledProcessError:
            return False

        # Run the test playbooks for each of the machine's components
        # (plus dependencies)
        for machine_component in machine_components:
            # Find the test directory for the component
            component_test_directory = os.path.join(
                defs.AnsiblePaths.component_tests_directory,
                machine_component)

            # Skip this component if it doesn't have a test directory
            if not os.path.exists(component_test_directory):
                continue

            # Find the playbooks with the test prefix in the directory
            test_prefix = defs.AnsiblePaths.component_test_playbook_prefix
            test_playbooks = utils.get_directory_yaml_files(
                component_test_directory, full_paths=True,
                filename_prefix=test_prefix)

            # Run each found test playbook for this component
            for test_playbook in test_playbooks:
                clean_name = os.path.basename(test_playbook)
                print(
                    f"[STAGE] Running {clean_name} "
                    f"from {machine_component} test directory"
                )
                try:
                    utils.run_playbook(
                        test_playbook,
                        inventory_path=f"{self.container_name},",
                        extra_vars=([f"container_name={self.container_name}",
                                     f"{defs.TEST_UUID_KEY}={self.uuid} "
                                     f"{defs.TEST_DOCKER_NETWORK_KEY}="
                                     f"{self.docker_network_name}"] +
                                    extra_vars),
                        user=defs.CONTAINER_TEST_USER,
                        force_venv_python=False,
                        verbose=self.verbose
                    )
                except subprocess.CalledProcessError:
                    return False

        # Run playbook on created docker container (in rollback mode)
        print(f"\n[STAGE] Test {self.machine_name} playbook in rollback mode")
        try:
            utils.run_playbook(
                self.actual_playbook_path,
                inventory_path=self.generated_inventories[1],
                limit=self.machine_name,
                user=defs.CONTAINER_TEST_USER,
                extra_vars=extra_vars + [f"{defs.TEST_UUID_KEY}={self.uuid}",
                                         f"{defs.TEST_DOCKER_NETWORK_KEY}="
                                         f"{self.docker_network_name}"],
                force_venv_python=False,
                verbose=self.verbose
            )
        except subprocess.CalledProcessError:
            return False

        # Signify test success
        return True
