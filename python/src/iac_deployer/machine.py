import os
import yaml

from . import definitions as defs
from . import utils


def get_machine_names():
    """
    Retrieves the names of all the machines. This is done by looking up
    the contents of the ansible/hosts/ directory.

    :return: The list of machine names
    :rtype: list of strs
    """
    machine_list = []
    for yaml_file in utils.get_directory_yaml_files(
            defs.AnsiblePaths.host_playbooks_directory):
        machine_list.append(os.path.splitext(yaml_file)[0])
    return machine_list


def parse_machine_playbook(machine_playbook_path, machine_name, quiet=False):
    """
    Parses the machine playbook that exists at machine_playbook_path.
    This involves retrieving what components (roles) the machine uses,
    and retrieving what operating system the machine should be running
    (via the os variable).

    :param machine_playbook_path: The path to the machine playbook YAML
        file to be parsed
    :type machine_playbook_path: str
    :param machine_name: The name of the machine
    :type machine_name: str
    :param quiet: If True, don't print any output apart from errors,
        defaults to False
    :type quiet: bool, optional
    :return: The found operating system for the machine and the
        components that the machine uses
    :rtype: tuple of a str and a list of strs
    """
    # Load the YAML from the machine playbook file and find the
    # value of the os variable
    with open(machine_playbook_path, 'r') as file_stream:
        machine_playbook_yaml = yaml.safe_load(file_stream)

    # Find the value of the os variable and parse the list of roles
    # from the machine playbook YAML
    machine_os = None
    machine_components = set()
    for play in machine_playbook_yaml:
        # If there are defined roles for this play, add their names
        # to machine_components
        if ('roles' in play and isinstance(play['roles'], list)):
            machine_components = machine_components.union(
                set(utils.parse_role_list(play['roles'])))
        # Attempt to find the os variable for this play
        if ('hosts' in play
                and 'vars' in play
                and play['hosts'] == machine_name
                and isinstance(play['vars'], dict)
                and 'os' in play['vars']):
            # Set the machine_os to the found os variable from the
            # playbook
            machine_os = play['vars']['os']

    if not quiet and machine_components:
        print(
            f"[INFO] Found components for {machine_name}: "
            f"{', '.join(machine_components)}"
        )
    sub_dependencies = set()
    for machine_component in machine_components:
        sub_dependencies = sub_dependencies.union(
            utils.get_role_dependencies(machine_component,
                                        quiet_output=True))
    # machine_components will now store the top-level components
    # plus all their dependencies
    machine_components = list(machine_components.union(sub_dependencies))
    machine_components.sort()

    return machine_os, machine_components
