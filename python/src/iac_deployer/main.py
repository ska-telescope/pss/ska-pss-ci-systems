import argparse
import os
import sys

from . import colours
from . import component_test
from . import definitions as defs
from . import deploy
from . import machine_test
from . import status
from . import utils


def list_component_tests(args):
    """
    Trigger for the print_component_tests function.

    :param args: argparse arguments (unused)
    :type args: argparse.Namespace
    """
    component_test.print_component_tests()


def test_component(args):
    """
    Trigger for running a component test. Sets the program exit code
    depending on if the test passed or failed.

    :param args: argparse arguments
    :type args: argparse.Namespace
    """
    if args.component:
        test = component_test.ComponentTest(
            args.component,
            os_list=args.os,
            verbose=args.verbose,
            force_docker_install=args.force_docker_install,
            pause_before_teardown=args.pause_before_teardown)
        if test.run():
            sys.exit(0)
        else:
            sys.exit(1)
    else:
        try:
            component_test.check_component_tests()
        except component_test.MissingComponentTestsError as err:
            print(f"{colours.FAIL}{err}{colours.CLEAR}")
            sys.exit(1)
        for component in component_test.get_component_tests():
            test = component_test.ComponentTest(
                component,
                os_list=args.os,
                verbose=args.verbose,
                force_docker_install=args.force_docker_install,
                pause_before_teardown=args.pause_before_teardown)
            if not test.run():
                sys.exit(1)
        sys.exit(0)


def list_machine_tests(args):
    """
    Trigger for the print_machine_tests function.

    :param args: argparse arguments (unused)
    :type args: argparse.Namespace
    """
    machine_test.print_machine_tests()


def test_machine(args):
    """
    Trigger for running a test of a machine's configuration. Sets the
    program exit code depending on if the test passed or failed.

    :param args: argparse arguments
    :type args: argparse.Namespace
    """
    if args.machine:
        test = machine_test.MachineTest(
            args.machine,
            verbose=args.verbose,
            force_docker_install=args.force_docker_install,
            pause_before_teardown=args.pause_before_teardown)
        if test.run():
            sys.exit(0)
        else:
            sys.exit(1)
    else:
        for machine in machine_test.get_machine_tests():
            test = machine_test.MachineTest(
                machine,
                verbose=args.verbose,
                force_docker_install=args.force_docker_install,
                pause_before_teardown=args.pause_before_teardown)
            if not test.run():
                sys.exit(1)
        sys.exit(0)


def machines_status(args):
    """
    Trigger for printing the status of the specified machine(s).

    :param args: argparse arguments
    :type args: argparse.Namespace
    """
    try:
        status.print_machines_status(args.machine, args.detailed)
    except (utils.UnknownMachineNameError, status.GitCommandError,
            utils.FailedGitChecks) as err:
        print(f"{colours.FAIL}{err}{colours.CLEAR}")
        sys.exit(1)


def deploy_machines(args):
    """
    Trigger for initiating rollback(s) of / deployment(s) to the
    specified machine(s).

    :param args: argparse arguments
    :type args: argparse.Namespace
    """
    if not deploy.deploy(args.machine, args.verbose,
                         ask_ssh_pass=args.ask_pass, quiet=args.quiet,
                         rollback_only=args.rollback_only):
        sys.exit(1)
    sys.exit(0)


def main():
    """
    Main program entry point. Parses command line arguments and calls
    the appropriate function.
    """
    # Change the working directory to be the repository's root so, for
    # example, git commands for the repository work correctly
    os.chdir(defs.REPOSITORY_ROOT_DIR)

    # Initialise AnsiblePaths to set up path definitions for the
    # ansible directory
    defs.AnsiblePaths.initialise()

    # Initialise PythonPaths to set up path definitions for Python
    # related files / directories
    defs.PythonPaths.initialise()

    # Set up argument parser for creating the command line interface
    parser = argparse.ArgumentParser(description='Interface to initiate '
                                     'testing and deployment of the defined '
                                     'Ansible configurations from the '
                                     'repository.', prog='iac_deployer')
    subparsers = parser.add_subparsers(dest='cmd', help='Sub-command help')
    subparsers.required = True

    # Create the component_tests parser
    parser_component_tests = subparsers.add_parser(
        'component_tests', help='List or run component tests')
    component_tests_subparsers = parser_component_tests.add_subparsers(
        dest='cmd', help='sub-command help')
    component_tests_subparsers.required = True

    # List component tests subparser
    parser_list_component_tests = component_tests_subparsers.add_parser(
        'list', help='List available component tests')
    parser_list_component_tests.set_defaults(func=list_component_tests)

    # Run component test subparser
    parser_run_component_test = component_tests_subparsers.add_parser(
        'run', help='Run a component test')
    parser_run_component_test.add_argument(
        'component', type=str, help='The name of the component to be tested',
        nargs='?')
    parser_run_component_test.add_argument(
        '-o', '--os', type=str, nargs='*', help='The operating system(s) to '
        'test the component on e.g. ubuntu-20.04')
    parser_run_component_test.add_argument(
        '-f', '--force-docker-install', help='Don\'t prompt if Docker needs '
        'installing - just install it', action='store_true')
    parser_run_component_test.add_argument(
        '-v', '--verbose', help='Increase output verbosity',
        action='store_true')
    parser_run_component_test.add_argument(
        '-p', '--pause-before-teardown',
        help='Wait for user input before tearing down the test(s)',
        action='store_true')
    parser_run_component_test.set_defaults(func=test_component)

    # Create the machine_tests parser
    parser_machine_tests = subparsers.add_parser(
        'machine_tests', help='List or run tests for machine configurations')
    machine_tests_subparsers = parser_machine_tests.add_subparsers(
        dest='cmd', help='sub-command help')
    machine_tests_subparsers.required = True

    # List machine configs subparser
    parser_list_machine_tests = machine_tests_subparsers.add_parser(
        'list', help='List available machine configurations that can be '
        'tested')
    parser_list_machine_tests.set_defaults(func=list_machine_tests)

    # Run machine test subparser
    parser_run_machine_test = machine_tests_subparsers.add_parser(
        'run', help='Run a test for a machine configuration')
    parser_run_machine_test.add_argument(
        'machine', type=str, help='The name of the machine that will have its'
        ' configuration tested', nargs='?')
    parser_run_machine_test.add_argument(
        '-f', '--force-docker-install', help='Don\'t prompt if Docker needs '
        'installing - just install it', action='store_true')
    parser_run_machine_test.add_argument(
        '-v', '--verbose', help='Increase output verbosity',
        action='store_true')
    parser_run_machine_test.add_argument(
        '-p', '--pause-before-teardown',
        help='Wait for user input before tearing down the test(s)',
        action='store_true')
    parser_run_machine_test.set_defaults(func=test_machine)

    # Create the machines parser
    parser_machines = subparsers.add_parser(
        'machines',
        help='Perform or check machine deployments, including rollbacks')
    machines_subparsers = parser_machines.add_subparsers(
        dest='cmd', help='sub-command help')
    machines_subparsers.required = True

    # Deploy machines subparser
    parser_deploy_machines = machines_subparsers.add_parser(
        'deploy',
        help='Command to initiate rollbacks (if necessary), followed by '
             'deployments of machine configurations'
    )
    parser_deploy_machines.add_argument(
        'machine',
        type=str,
        help='The name of the machine(s) to rollback / deploy to',
        nargs='+'
    )
    parser_deploy_machines.add_argument(
        '--rollback-only', help='Only perform rollback(s) of the '
        'configuration(s) - skip deployment(s)', action='store_true')
    parser_deploy_machines.add_argument(
        '-k', '--ask-pass', help='Force Ansible to prompt you for the SSH '
        'password(s) to connect to the target machine(s) with',
        action='store_true')
    parser_deploy_machines.add_argument(
        '-v', '--verbose', help='Increase output verbosity',
        action='store_true')
    parser_deploy_machines.add_argument(
        '-q', '--quiet', help='Suppress all output apart from Ansible output',
        action='store_true')
    parser_deploy_machines.set_defaults(func=deploy_machines)

    # Machines status subparser
    parser_machines_status = machines_subparsers.add_parser(
        'status',
        help='Command to check the status of machines'
    )
    parser_machines_status.add_argument(
        'machine',
        type=str,
        help='The name of the machine(s) to check the status of',
        nargs='*'
    )
    parser_machines_status.add_argument(
        '-d', '--detailed',
        help='Show detailed status information, rather than just a summary',
        action='store_true')
    parser_machines_status.set_defaults(func=machines_status)

    args = parser.parse_args()
    args.func(args)


if __name__ == '__main__':
    main()
