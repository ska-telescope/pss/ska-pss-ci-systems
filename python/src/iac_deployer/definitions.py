import os

REPOSITORY_ROOT_DIR = os.path.abspath(os.path.join(os.path.dirname(
    os.path.abspath(__file__)), '../../../'))
YAML_EXTENSIONS = ['.yml', '.yaml']
ANSIBLE_CMD = 'ansible'
ANSIBLE_PLAYBOOK_CMD = 'ansible-playbook'
CONTAINER_TEST_USER = 'ci-test-user'
TEST_UUID_KEY = 'iac_deployer_test_uuid'
TEST_DOCKER_NETWORK_KEY = 'iac_deployer_test_docker_network'
TEST_DOCKER_NETWORK_VALUE_PREFIX = 'iac_deployer_test_'

# This variable remembers if unit tests are being run at the moment.
# It is set to True in python/tests/conftest.py
UNIT_TESTS = False


class Docker:
    """
    Stores definitions related to Docker
    """
    UNIX_SOCKET_FILE = '/var/run/docker.sock'
    UNIX_SOCKET = 'unix:/' + UNIX_SOCKET_FILE
    DOCKER_HOST_TLS = 'tcp://docker:2376'
    DOCKER_HOST_NO_TLS = 'tcp://docker:2375'
    API_DIFF_PATH_KEY = 'Path'
    API_DIFF_KIND_KEY = 'Kind'
    API_DIFF_MODIFIED_VALUE = 0
    API_DIFF_ADDED_VALUE = 1
    API_DIFF_DELETED_VALUE = 2

    def get_url():
        """
        Get the URL to either the Docker socket if it exists or the
        default Docker host (if using a network connection.)

        :return: The URL to either the Unix socket or the URL of the
            network connection
        :rtype: str
        """
        if os.path.exists(Docker.UNIX_SOCKET_FILE):
            return Docker.UNIX_SOCKET
        return Docker.DOCKER_HOST_TLS


class PythonPaths:
    """
    Stores paths for accessing Python related files or directories
    """

    @staticmethod
    def initialise(custom_python_binary=None):
        """
        Initialises path definitions for Python related files /
        directories.

        :param custom_python_binary: If specified, use this path for
            finding the Python binary to use, defaults to None
        :type custom_python_binary: str, optional
        :raises FileNotFoundError: If the Python binary or activate
            binary couldn't be found
        """
        if custom_python_binary:
            PythonPaths.VENV_PYTHON_BINARY = custom_python_binary
            PythonPaths.VENV_ACTIVATE_BINARY = os.path.join(
                os.path.dirname(custom_python_binary), 'activate')
        else:
            PythonPaths.VENV_PYTHON_BINARY = os.path.join(REPOSITORY_ROOT_DIR,
                                                          'venv/bin/python')
            PythonPaths.VENV_ACTIVATE_BINARY = os.path.join(
                REPOSITORY_ROOT_DIR, 'venv/bin/activate')

        if not os.path.exists(PythonPaths.VENV_PYTHON_BINARY):
            raise FileNotFoundError(
                'Couldn\'t find Python binary in virtual environment: '
                f"{PythonPaths.VENV_PYTHON_BINARY}")
        if not os.path.exists(PythonPaths.VENV_ACTIVATE_BINARY):
            raise FileNotFoundError(
                'Couldn\'t find activate binary in virtual environment: '
                f"{PythonPaths.VENV_ACTIVATE_BINARY}")


class AnsiblePaths:
    """
    Stores paths for accessing areas in the Ansible data directory
    """

    @staticmethod
    def initialise(custom_root_dir=None):
        """
        Initialises path definitions for the Ansible directory.

        :param custom_root_dir: If specified, use this directory as the
            root Ansible directory that the other paths will derive
            from. Defaults to None
        :type custom_root_dir: str, optional
        """
        # Use the custom_root_dir, if specified
        if custom_root_dir:
            AnsiblePaths.root_dir = custom_root_dir
        else:
            AnsiblePaths.root_dir = os.path.abspath(os.path.join(
                os.path.dirname(os.path.abspath(__file__)),
                '../../../ansible'))

        # Set paths relative to AnsiblePaths.root_dir
        AnsiblePaths.inventory_file = os.path.join(AnsiblePaths.root_dir,
                                                   'production')
        AnsiblePaths.host_playbooks_directory = os.path.join(
            AnsiblePaths.root_dir, 'hosts/')
        AnsiblePaths.group_playbooks_directory = os.path.join(
            AnsiblePaths.root_dir, 'groups/')
        AnsiblePaths.example_playbooks_directory = os.path.join(
            AnsiblePaths.root_dir, 'examples/')
        AnsiblePaths.roles_directory = os.path.join(AnsiblePaths.root_dir,
                                                    'roles/')
        AnsiblePaths.roles_meta_main_file = 'meta/main.yml'
        AnsiblePaths.os_dockerfiles_directory = os.path.join(
            AnsiblePaths.root_dir, 'roles/os-containerised/files/')
        AnsiblePaths.tests_directory = os.path.join(AnsiblePaths.root_dir,
                                                    'tests/')

        # Docker playbooks for testing definitions
        from . import utils
        try:
            path_to_file = utils.get_directory_yaml_file(
                AnsiblePaths.tests_directory, 'docker-install')
            AnsiblePaths.docker_install_playbook = path_to_file
        except FileNotFoundError as err:
            print('Could not find the docker-install playbook in '
                  f"{AnsiblePaths.tests_directory}! Please create it and try "
                  'again.')
            raise err
        try:
            path_to_file = utils.get_directory_yaml_file(
                AnsiblePaths.tests_directory, 'docker-create')
            AnsiblePaths.docker_create_playbook = path_to_file
        except FileNotFoundError as err:
            print('Could not find the docker-create test playbook in '
                  f"{AnsiblePaths.tests_directory}! Please create it and try "
                  'again.')
            raise err
        try:
            path_to_file = utils.get_directory_yaml_file(
                AnsiblePaths.tests_directory, 'docker-destroy')
            AnsiblePaths.docker_destroy_playbook = path_to_file
        except FileNotFoundError as err:
            print('Could not find the docker-destroy test playbook in '
                  f"{AnsiblePaths.tests_directory}! Please create it and try "
                  'again.')
            raise err

        # Machine tests definitions
        AnsiblePaths.machine_tests_directory = os.path.join(
            AnsiblePaths.root_dir, 'tests/machine/')
        AnsiblePaths.machine_test_vars_file = 'vars'
        AnsiblePaths.machine_test_setup_file = 'setup'
        AnsiblePaths.machine_test_teardown_file = 'teardown'

        # Component tests definitions
        AnsiblePaths.component_tests_directory = os.path.join(
            AnsiblePaths.root_dir, 'tests/components/')
        AnsiblePaths.component_test_playbook_prefix = 'test_'
        AnsiblePaths.component_test_setup_file = 'setup'
        AnsiblePaths.component_test_deploy_file = 'deploy'
        AnsiblePaths.component_test_vars_file = 'vars'
        AnsiblePaths.component_test_rollback_file = 'rollback'
        AnsiblePaths.component_test_teardown_file = 'teardown'
        AnsiblePaths.global_ignore_lists_directory = os.path.join(
            AnsiblePaths.root_dir, 'tests/ignore_lists/'
        )
        AnsiblePaths.component_test_ignore_list = 'ignore_list'
