import getpass
import queue
import subprocess
import threading

from . import definitions as defs


def stream_reader(stream, queue, exit_event=None):
    """
    Reads lines from the provided stream and adds them to the provided
    queue until the stream ends or the user requests an exit via
    exit_event

    :param stream: The stream to read lines from
    :type stream: io.BufferedReader
    :param queue: The queue to add the lines to
    :type queue: queue.Queue
    :param exit_event: If this event is set, then the lines will stop
    being read and the loop will exit, defaults to None
    :type exit_event: threading.Event, optional
    """
    stream_name = str(stream)
    try:
        for line in iter(stream.readline, b''):
            queue.put((stream_name, line.decode()))
            # Check if user has signalled to exit
            if exit_event and exit_event.is_set():
                break
    finally:
        queue.put(None)  # Signal end of stream


def execute(cmd, verbose=False, print_command_when_verbose=True, env=None,
            new_login=False):
    """
    Executes a command via subprocess.Popen()

    :param cmd: The command to execute e.g. ['ls', '-la']
    :type cmd: list
    :param verbose: If True, prints what command is being run and
        prints the output of the command as it is running. If False,
        the output of the command will only be printed if the command
        reports an error. Defaults to False
    :type verbose: bool, optional
    :param print_command_when_verbose: If False, don't print what
        command is being run when in verbose mode, defaults to True
    :type print_command_when_verbose: bool, optional
    :param env: Used to send custom environment variables to the
        created subprocess, defaults to None
    :type env: dict of strs, optional
    :param new_login: If True, run the command after logging in again
        with su. NOTE: requires sudo. Defaults to False
    :type new_login: bool, optional
    :raises subprocess.CalledProcessError: If the command reports a
        non-zero exit code
    :return: The stdout, stderr, and stdout & stderr combined output,
        as a tuple
    :rtype: tuple containing strs
    """
    if new_login:
        cmd_as_str = ' '.join(cmd)
        cmd_as_str = (f"source {defs.PythonPaths.VENV_ACTIVATE_BINARY} && "
                      f"{cmd_as_str}")
        cmd = ['sudo', '-E', 'su', '--preserve-environment', '-c', cmd_as_str,
               '-', getpass.getuser()]

    if verbose and print_command_when_verbose:
        print(f"Running command: {str(cmd)}")
    process = subprocess.Popen(cmd, env=env, stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)
    q = queue.Queue()
    exit_event = threading.Event()
    threading.Thread(target=stream_reader,
                     args=[process.stdout, q, exit_event]).start()
    threading.Thread(target=stream_reader,
                     args=[process.stderr, q, exit_event]).start()
    stdout_name = str(process.stdout)
    stderr_name = str(process.stderr)
    stdout_output = stderr_output = combined_output = ''
    return_code = None
    try:
        for _ in range(2):
            for stream_name, line in iter(q.get, None):
                if verbose:
                    print(line, end='')
                if stream_name == stdout_name:
                    stdout_output += line
                elif stream_name == stderr_name:
                    stderr_output += line
                combined_output += line
        return_code = process.wait()
    except KeyboardInterrupt:
        print('\nPolite exit')
        exit_event.set()
        return_code = 1
    process.stdout.close()
    process.stderr.close()
    if return_code:
        if not verbose:
            print(combined_output, end='')
        raise subprocess.CalledProcessError(
            returncode=return_code,
            cmd=cmd,
            output=combined_output,
            stderr=stderr_output)
    return stdout_output, stderr_output, combined_output
