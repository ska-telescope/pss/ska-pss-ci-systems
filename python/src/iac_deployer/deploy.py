import os
import subprocess
import tempfile

from . import colours
from . import definitions as defs
from . import machine
from . import process as proc
from . import status
from . import utils


def deploy(machine_list, verbose=False, rollback_only=False,
           ask_ssh_pass=False, quiet=False, is_clean_testing_output=None,
           is_unpushed_testing_output=None, is_git_repository_override=None):
    """
    Initiates machine deployments / rollbacks by creating
    MachineDeployment objects.

    :param machine_list: The list of machines to deploy / rollback. If
        empty, all machines will be deployed / rolled back
    :type machine_list: list of strs
    :param verbose: If True, enable verbose output, defaults to False
    :type verbose: bool, optional
    :param rollback_only: If True, only perform machine rollbacks (skip
        deployments), defaults to False
    :type rollback_only: bool, optional
    :param ask_ssh_pass: If True, force Ansible to prompt the user for
        SSH passwords when connecting to the target machines, defaults
        to False
    :type ask_ssh_pass: bool, optional
    :param quiet: If True, only print Ansible output and error
        messages, defaults to False
    :type quiet: bool, optional
    :param is_clean_testing_output: If set, use this in place of the
        actual git output for the repository clean check, defaults to
        None
    :type is_clean_testing_output: str, optional
    :param is_unpushed_testing_output: If set, use this in place of the
        actual git output for the unpushed commits check, defaults to
        None
    :type is_unpushed_testing_output: str, optional
    :param is_git_repository_override: If set, use this in place of the
        actual git repository check result, defaults to None
    :type is_git_repository_override: bool, optional
    :return: True on success, False on failure
    :rtype: bool
    """
    if verbose and quiet:
        print(
            f"{colours.FAIL}--verbose and --quiet are mutually exclusive"
            f"{colours.CLEAR}"
        )
        return False
    try:
        utils.check_for_git_issues(
            is_clean_testing_output, is_unpushed_testing_output,
            is_git_repository_override)
    except utils.FailedGitChecks as err:
        print(f"{colours.FAIL}{err}{colours.CLEAR}")
        return False
    # If no machine list supplied, deploy to all machines
    if not machine_list:
        machine_list = machine.get_machine_names()

    for machine_name in machine_list:
        try:
            deployment = MachineDeployment(
                machine_name, verbose=verbose, ask_ssh_pass=ask_ssh_pass,
                quiet=quiet)
        except FileNotFoundError as err:
            print(f"{colours.FAIL}{err}{colours.CLEAR}")
            print(f"Please create a playbook for the '{machine_name}' "
                  'machine using the provided template in '
                  f"{defs.AnsiblePaths.example_playbooks_directory}")
            return False
        except status.GitCommandError as err:
            print(f"{colours.FAIL}{err}{colours.CLEAR}")
            print('Please check the error and try again.')
            return False
        except utils.InventoryMissingHostError as err:
            print(f"{colours.FAIL}{err}{colours.CLEAR}")
            print(
                f"Please add an entry to the inventory for machine "
                f"'{machine_name}' and try again."
            )
            return False

        try:
            deployment.rollback()
        except MachineDeployment.FailedRollbackError as err:
            if not quiet:
                print(f"{colours.FAIL}{err}{colours.CLEAR}")
                if not rollback_only:
                    print('Aborting deployment due to rollback errors.')
            return False
        except Exception as err:
            if not rollback_only:
                print(
                    'Aborting deployment due to rollback errors. See below:'
                )
            raise err

        if not rollback_only:
            try:
                deployment.deploy()
            except MachineDeployment.FailedDeploymentError as err:
                if not quiet:
                    print(f"{colours.FAIL}{err}{colours.CLEAR}")
                return False
            except Exception as err:
                print(
                    f"An exception occurred during '{machine_name}' "
                    'deployment. Details:'
                )
                raise err

    return True


class MachineDeployment:
    """
    Class defining a deployment to a particular machine.
    """

    class FailedRollbackError(Exception):
        """
        Exception raised if an error occurred while performing a
        rollback.
        """
        pass

    class FailedDeploymentError(Exception):
        """
        Exception raised if an error occurred while performing a
        deployment.
        """
        pass

    def __init__(self, machine_name, verbose=False, ask_ssh_pass=False,
                 quiet=False):
        """
        Constructor for the new machine deployment.

        :param machine_name: The name of the machine to deploy to
            (without any .yml or .yaml extension)
        :type machine_name: str
        :param verbose: Whether to show verbose output for the
            deployment or not, defaults to False
        :type verbose: bool, optional
        :param ask_ssh_pass: If True, force Ansible to prompt the user
            for the SSH password when connecting to the target machine,
            defaults to False
        :type ask_ssh_pass: bool, optional
        :param quiet: If True, suppress all output apart from Ansible
            output, defaults to False
        :type quiet: bool, optional
        :raises FileNotFoundError: If the YAML playbook file for the
            machine name could not be found
        """
        self.machine_name = machine_name
        self.verbose = verbose
        self.ask_ssh_pass = ask_ssh_pass
        self.quiet = quiet
        # Generate required inventory files for deployment & rollback
        self._generate_inventory_files()
        # Fetch machine playbook
        self.playbook_path = utils.get_directory_yaml_file(
            defs.AnsiblePaths.host_playbooks_directory,
            self.machine_name
        )
        self.machine_status = status.MachineStatus(machine_name, quiet=quiet)

    def _print(self, msg):
        """
        Prints a message if quiet mode isn't enabled.

        :param msg: The message to print
        :type msg: str
        """
        if not self.quiet:
            print(msg)

    def _generate_inventory_files(self):
        """
        Generates the required inventory files. The files will be the
        same as the original inventory file from the repository, except
        the entry for the machine in both files will have a new host
        variable appended called deploy_mode, which will be set to
        deploy in the first file, and rollback in the second.

        The files can then be used as the inventory sources for the
        machine deployment and rollback.

        :raises utils.InventoryMissingHostError: if the original
            inventory file is missing an entry for the machine
        """
        self.generated_inventories = []
        for mode in ['deploy_mode=deploy', 'deploy_mode=rollback']:
            self.generated_inventories.append(
                utils.gen_inventory_with_altered_vars(
                    [self.machine_name], [mode], [mode])
            )

    def _initiate_git_archive_rollback(self):
        """
        Initiates a rollback by first generating a tar archive of
        the repository at the retrieved commit SHA from the status
        repository, extracting the archive to a temporary directory,
        and then running iac_deployer from within that temporary
        directory to initiate the rollback for that commit.

        The rollback is only performed if the machine's status is
        appropriate. Once the rollback has completed, the machine's
        status is updated in the status repository. The status will
        vary if the rollback failed or was successful.

        :raises MachineDeployment.FailedRollbackError: If the rollback
            failed, as determined by the return code of the
            iac_deployer subprocess.
        """
        self._print(
            f"[STAGE] Initiating rollback for '{self.machine_name}' machine"
        )
        if not self.machine_status.can_rollback():
            self._print(
                f"{colours.WARNING}[WARNING] Skipping rollback for machine "
                f"'{self.machine_name}' - inappropriate status: "
                f"'{self.machine_status.get_status()}'{colours.CLEAR}"
            )
            return
        if (self.machine_status.get_status() ==
                status.StatusValues.FAILED_ROLLBACK):
            commit_sha_to_use = utils.get_current_git_commit_sha()
        else:
            commit_sha_to_use = self.machine_status.get_commit_sha()
        self._print(f"Extracting git contents for SHA '{commit_sha_to_use}'")
        git_archive_process = subprocess.Popen(
            ['git', 'archive', '--format', 'tar', commit_sha_to_use],
            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        with tempfile.TemporaryDirectory() as temp_dir:
            tar_process = subprocess.Popen(
                ['tar', '-C', temp_dir, '-xf', '-'],
                stdin=git_archive_process.stdout,
                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            for process in [git_archive_process, tar_process]:
                return_code = process.wait()
                if return_code:
                    raise subprocess.CalledProcessError(
                        returncode=process.returncode,
                        cmd=process.args,
                        output=process.stdout.read().decode(),
                        stderr=process.stderr.read().decode()
                    )
            self._print(
                '[STAGE] Rolling back extracted archive\'s configuration for '
                f"'{self.machine_name}' machine"
            )
            self._print(
                'Please be patient. The extracted archive will need to '
                'create its own virtual environment.'
            )
            # Set the IAC_QUIET_OUTPUT environment variable to suppress
            # output from pip etc. from iac_deployer
            my_env = os.environ.copy()
            my_env['IAC_QUIET_OUTPUT'] = 'true'
            log_timestamp = status.MachineStatus.get_ansible_log_timestamp()
            rollback_command = [
                temp_dir + '/iac_deployer', 'machines', 'deploy',
                self.machine_name, '--rollback-only', '--quiet'
            ]
            if self.ask_ssh_pass:
                rollback_command.append('--ask-pass')
            try:
                _, _, combined_output = proc.execute(
                    rollback_command,
                    env=my_env, verbose=True,
                    print_command_when_verbose=False)
            except subprocess.CalledProcessError as err:
                self.machine_status.write(
                    commit_sha_to_use, status.StatusValues.FAILED_ROLLBACK,
                    ansible_log=err.output,
                    ansible_log_timestamp=log_timestamp)
                self.machine_status.print_user_friendly_status()
                raise MachineDeployment.FailedRollbackError(
                    '[ERROR] Rollback failed for machine '
                    f"'{self.machine_name}'!"
                )
            self._print(
                f"{colours.SUCCESS}Successfully rolled back extracted "
                f"archive's configuration for '{self.machine_name}' machine"
                f"{colours.CLEAR}"
            )
        self.machine_status.write(
            None,
            status.StatusValues.NOT_DEPLOYED,
            ansible_log=combined_output,
            ansible_log_commit_sha=commit_sha_to_use,
            ansible_log_timestamp=log_timestamp
        )

    def _rollback_this_config(self):
        """
        Runs the generated machine playbook in rollback mode from
        within THIS repository.

        :raises MachineDeployment.FailedRollbackError: If the ansible-
            playbook process reports a non-zero return code
        """
        playbook_file = None
        try:
            playbook_file = utils.concatenate_playbooks(self.machine_name,
                                                        quiet=self.quiet)
        except SyntaxError as err:
            self._print(
                'Please check the syntax of the machine and group '
                'playbooks that are being used in rollback mode '
                'and try again. Full error details:'
            )
            raise err
        except Exception as err:
            self._print(
                'An exception occurred when generating the rollback '
                'playbook, please see details below:'
            )
            raise err

        # Run the generated playbook file in rollback mode,
        # limiting to the machine
        self._print(
            f"[STAGE] Running playbook for '{self.machine_name}' machine in "
            'rollback mode...'
        )
        try:
            utils.run_playbook(
                playbook_file,
                local=False,
                inventory_path=self.generated_inventories[1],
                limit=self.machine_name,
                ask_become_pass=True,
                ask_ssh_pass=self.ask_ssh_pass,
                verbose=self.verbose
            )
        except subprocess.CalledProcessError:
            raise MachineDeployment.FailedRollbackError(
                '[ERROR] Rollback playbook failed for machine '
                f"'{self.machine_name}'!"
            )
        finally:
            os.remove(playbook_file)

    def rollback(self):
        """
        Initiates a rollback. If the .git directory exists, then a
        tar archive is generated for a particular commit and the
        iac_deployer script from within that extracted archive is used
        to perform the rollback. Otherwise, if the .git directory
        doesn't exist, then it is assumed that this repository has been
        generated via the git archive method and therefore runs the
        ansible-playbook command to actually perform the rollback.
        """
        if not defs.UNIT_TESTS and utils.is_git_repository():
            self._initiate_git_archive_rollback()
        else:
            self._rollback_this_config()

    def deploy(self):
        """
        Performs a deployment by running the ansible-playbook command
        for the machine's generated playbook.

        The deployment is only performed if the machine's status is
        appropriate.

        :raises SyntaxError: If the machine and relevant group
            playbook(s) have a YAML syntax error in them
        :raises MachineDeployment.FailedDeploymentError: If the
            ansible-playbook command returns with a non-zero exit code
        """
        self._print(
            f"[STAGE] Initiating deployment for '{self.machine_name}' machine"
        )
        if not self.machine_status.can_deploy():
            self._print(
                f"{colours.WARNING}[WARNING] Skipping deployment for machine "
                f"'{self.machine_name}' - inappropriate status: "
                f"'{self.machine_status.get_status()}'{colours.CLEAR}"
            )
            return
        playbook_file = None
        try:
            playbook_file = utils.concatenate_playbooks(self.machine_name,
                                                        quiet=self.quiet)
        except SyntaxError as err:
            self._print(
                'Please check the syntax of the machine and group '
                'playbooks and try again. Full error details:'
            )
            raise err
        except Exception as err:
            self._print(
                'An exception occurred when generating the deployment '
                'playbook, please see details below:'
            )
            raise err

        # Run the generated playbook file in deploy mode,
        # limiting to the machine
        self._print(
            f"[STAGE] Running playbook for '{self.machine_name}' machine in "
            'deploy mode...'
        )
        log_timestamp = status.MachineStatus.get_ansible_log_timestamp()
        try:
            _, _, combined_output = utils.run_playbook(
                playbook_file,
                local=False,
                inventory_path=self.generated_inventories[0],
                limit=self.machine_name,
                ask_become_pass=True,
                ask_ssh_pass=self.ask_ssh_pass,
                verbose=self.verbose
            )
        except subprocess.CalledProcessError as err:
            self.machine_status.write(
                utils.get_current_git_commit_sha(),
                status.StatusValues.FAILED_DEPLOYMENT,
                ansible_log=err.output,
                ansible_log_timestamp=log_timestamp
            )
            self.machine_status.print_user_friendly_status()
            raise MachineDeployment.FailedDeploymentError(
                '[ERROR] Deployment failed for machine '
                f"'{self.machine_name}'!"
            )
        else:
            self._print(
                f"{colours.SUCCESS}Deployment successful for "
                f"'{self.machine_name}' machine{colours.CLEAR}"
            )
            self.machine_status.write(
                utils.get_current_git_commit_sha(),
                status.StatusValues.DEPLOYED,
                ansible_log=combined_output,
                ansible_log_timestamp=log_timestamp
            )
        finally:
            os.remove(playbook_file)

    def __del__(self):
        """
        Object destructor.

        Deletes the generated inventory files.
        """
        # Remove the generated inventories
        for inventory in self.generated_inventories:
            if os.path.isfile(inventory):
                os.remove(inventory)
