import json
import os
import re
import shutil
import subprocess
import tempfile
import textwrap
import yaml

from . import colours
from . import definitions as defs
from . import process as proc


class UnknownMachineNameError(Exception):
    """
    Exception raised when a machine name is referenced that doesn't
    exist.
    """
    def __init__(self, machine_name):
        msg = (
            f"Unknown machine name '{machine_name}'. Please check it and try "
            'again.'
        )
        super().__init__(msg)


def check_sudo():
    """
    Checks if sudo is installed or not.

    :return: True if sudo is installed, False otherwise
    :rtype: bool
    """
    if shutil.which('sudo'):
        return True
    return False


class DockerPermissionError(Exception):
    """
    Exception raised when Docker is installed, but can't run
    "docker ps" (most likely due to a permission issue).
    """
    pass


def print_docker_permissions_error():
    """
    Prints an error message that should be printed if a Docker
    permissions issue was detected.
    """
    msgs = [
        '''
        An error occurred when running the docker ps command. Are you
        in the docker group and, if necessary, have you logged out of
        your shell and logged back in again to re-evaluate group
        permissions?
        ''',
        '''
        The tests need to be able to run docker commands without root
        privileges / sudo.
        ''']
    for msg in msgs:
        print(textwrap.fill(textwrap.dedent(msg).strip()) + '\n')


def check_docker_permissions(override=None):
    """
    Checks that "docker ps" can be run with no permission issues.

    :param override: If True or False, return that value in place of
        the actual result (useful for unit testing), defaults to None
    :type override: bool, optional
    :return: True if there were no permission issues; False if there
        were
    :rtype: bool
    """
    if isinstance(override, bool):
        return override
    ps_result = subprocess.run(['docker', 'ps'], stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)
    return ps_result.returncode == 0


def is_docker_installed(override=None):
    """
    Checks if the docker command exists or not.

    :param override: If True or False, return that value in place of
        the actual result (useful for unit testing), defaults to None
    :type override: bool, optional
    :return: True if the docker command exists; False if it does not
    :rtype: bool
    """
    if isinstance(override, bool):
        return override
    if shutil.which('docker'):
        return True
    return False


def ensure_docker(force=False, no_sudo_prompt=False,
                  is_installed_override=None,
                  final_is_installed_override=None,
                  permission_check_override=None):
    """
    Checks if docker is installed, and, if it isn't, prompts the user
    to see if they wish to install it, and if they do then the
    docker-install.yml playbook is run.

    :param force: If True, don't prompt the user, defaults to False
    :type force: bool, optional
    :param no_sudo_prompt: If True, don't prompt the user for their
        sudo password when running the Docker install playbook,
        defaults to False
    :type no_sudo_prompt: bool, optional
    :param is_installed_override: If True or False, override the actual
        result from the initial Docker install check with this value
        (useful for unit testing). Defaults to None
    :type is_installed_override: bool, optional
    :param final_is_installed_override: If True or False, override the
        actual result from the final Docker install check that happens
        after the Docker install playbook has completed with this value
        (useful for unit testing). Defaults to None
    :type final_is_installed_override: bool, optional
    :param permission_check_override: If True or False, override the
        actual result from the Docker permission check that happens
        after the Docker install playbook has completed with this value
        (useful for unit testing). Defaults to None
    :type permission_check_override: bool, optional
    :return: True on success, False on failure
    :rtype: bool
    """
    if not is_docker_installed(is_installed_override):
        if force:
            choice = 'y'
        else:
            msg = (f"{colours.WARNING}[WARNING] Docker is not installed! "
                   'Install it now? (root / sudo privileges required) [Y/n] '
                   f"{colours.CLEAR}")
            choice = input(msg)
            choice = choice.strip().lower()
        if choice == 'y' or choice == 'yes':
            print('[STAGE] Install Docker')
            need_sudo = False
            if not no_sudo_prompt and not os.geteuid() == 0:
                # If we are not root, we need to use sudo
                need_sudo = True
                if not check_sudo():
                    print(f"{colours.FAIL}[ERROR] sudo isn't installed and "
                          'is needed for privilege escalation. Please '
                          f"install it and try again.{colours.CLEAR}")
                    return False
                print('Ansible will now prompt you for your password for use '
                      'with sudo. Please enter it.')
            try:
                run_playbook(defs.AnsiblePaths.docker_install_playbook,
                             ask_become_pass=need_sudo)
            except subprocess.CalledProcessError:
                print(f"{colours.FAIL}[ERROR] Could not install docker"
                      f"{colours.CLEAR}")
                return False

            if is_docker_installed(final_is_installed_override):
                print(f"{colours.SUCCESS}Docker installed successfully"
                      f"{colours.CLEAR}\n")
                if not check_docker_permissions(permission_check_override):
                    paragraph_1 = '''
                        You have been added to the docker group. In order for
                        your group membership to be re-evaluated so you can
                        use docker without sudo, you need to log out of
                        your shell and log back in. If you're using Linux,
                        you can also run the following command to re-evalute
                        group permissions:'''
                    paragraph_2 = '''
                        Then, verify that you can run docker without sudo:'''
                    paragraph_3 = '''
                        If that works, then try running the test again.
                        If it does not, you may need to reboot your machine
                        before trying again.'''
                    paragraph_4 = '''
                        The tests need to be able to use
                        docker without sudo since they don't normally have
                        root privileges.'''
                    final_msg = '\n\n'.join([
                        textwrap.fill(textwrap.dedent(paragraph_1).strip()),
                        " $ newgrp docker",
                        textwrap.fill(textwrap.dedent(paragraph_2).strip()),
                        " $ docker run hello-world",
                        textwrap.fill(textwrap.dedent(paragraph_3).strip()),
                        textwrap.fill(textwrap.dedent(paragraph_4).strip())
                    ]) + '\n'
                    raise DockerPermissionError(final_msg)
            else:
                print(f"{colours.FAIL}[ERROR] Docker still isn't installed! "
                      f"Please check the above Ansible output."
                      f"{colours.CLEAR}")
                return False
        else:
            print('Abort.')
            return False
    return True


def ensure_docker_mac_os():
    """
    Ensures that Docker is installed if we are running macOS.

    :return: True if we are running macOS and Docker is installed, True
        if we aren't running macOS, False if we are running macOS and
        Docker isn't installed.
    :rtype: bool
    """
    import sys
    if sys.platform.startswith('darwin'):
        # Check if docker is installed
        try:
            proc.execute(['docker', '--version'])
        except subprocess.CalledProcessError as err:
            print(colours.FAIL, end='')
            print('Could not verify that Docker is installed. Please ensure '
                  'you have installed Docker by following the instructions '
                  'in the readme (under the section "Installing Docker on '
                  'MacOS") and try again. Error details:')
            print(err)
            print(colours.CLEAR, end='')
            return False
    return True


def ensure_sudo(quiet=False):
    """
    Runs the sudo command with the -v flag to refresh sudo permissions.

    :param quiet: If True, only show necessary output, defaults to False
    :type quiet: bool, optional
    :raises PermissionError: If there was an error when running the
        sudo command
    """
    try:
        proc.execute(['sudo', '-v'], verbose=True,
                     print_command_when_verbose=False)
    except subprocess.CalledProcessError:
        print(f"{colours.FAIL}[ERROR] Could not get sudo permissions! Please "
              f"try again.{colours.CLEAR}")
        raise PermissionError('Could not get sudo permissions!')


def get_directory_yaml_files(dir_path, filename_prefix=None, full_paths=False,
                             include_hidden=False):
    """
    Gets all YAML files contained in the specified directory

    :param dir_path: The full path to the directory
    :type dir_path: str
    :param filename_prefix: If set, only include files whose names
        start with this prefix, defaults to None
    :type filename_prefix: str, optional
    :param full_paths: If True, return the full path to each found file
        rather than just the filenames, defaults to False
    :type full_paths: bool, optional
    :param include_hidden: If True, also include hidden YAML files
        (whose filenames start with a dot), defaults to False
    :type include_hidden: bool, optional
    :return: List of YAML (*.yml, *.yaml) files from the directory (not
        their full path, just the name e.g. tengu.yml)
    :rtype: list of strs
    """
    result = []
    files = [f for f in os.listdir(dir_path) if os.path.isfile(
             os.path.join(dir_path, f))]
    for f in files:
        if f.startswith('.') and not include_hidden:
            continue
        if filename_prefix:
            if not f.startswith(filename_prefix):
                continue
        for ext in defs.YAML_EXTENSIONS:
            if f.endswith(ext):
                if full_paths:
                    result.append(os.path.join(dir_path, f))
                else:
                    result.append(f)
    return result


def get_directory_yaml_file(dir_path, filename):
    """
    Attempts to retrieve the path to a YAML file with the specified
    name in the specified directory.

    :param dir_path: Path to the directory that should contain the YAML
        file
    :type dir_path: str
    :param filename: The name of the YAML file to look for (without any
        .yml or .yaml extension)
    :type filename: str
    :raises FileNotFoundError: If neither the file with a .yml or .yaml
        extension could be found in the directory
    :return: The full path to the found file (with the .yml or .yaml
        extension)
    :rtype: str
    """
    found = False
    file_list = ''
    for i, ext in enumerate(defs.YAML_EXTENSIONS):
        # Update checked file list
        file_list += filename + ext
        if i < len(defs.YAML_EXTENSIONS) - 1:
            file_list += ', '
        # Check if file exists
        result = os.path.join(dir_path, filename + ext)
        if os.path.isfile(result):
            found = True
            break
    if not found:
        raise FileNotFoundError(f"Could not find any of {file_list} in "
                                f"{str(dir_path)}")
    return result


def load_yaml_file(file_path):
    """
    Loads a YAML file's contents and returns it.

    :param file_path: The path to the YAML file
    :type file_path: str
    :raises yaml.parser.ParserError: If the YAML is invalid
    :return: The loaded YAML
    :rtype: dict
    """
    try:
        with open(file_path, 'r') as file_stream:
            yaml_result = yaml.safe_load(file_stream)
    except yaml.parser.ParserError as err:
        print(f"{colours.FAIL}Invalid YAML! Please check "
              f"{file_path} and try again. Error details:"
              f"{colours.CLEAR}")
        raise err
    return yaml_result


def get_unique_file_path(file_path):
    """
    Finds a unique file path based on the provided one. If a file
    already exists at the file path, then an underscore followed by a
    number is appended and the check to see if a file exists already at
    that path is performed again. This is repeated, incrementing the
    number each time, until no file exists already at the path i.e. the
    file path is unique.

    :param file_path: The (full) ideal file path
    :type file_path: str
    :return: The first available file path where a file doesn't already
        exist, based on the input file path
    :rtype: str
    """
    filename, extension = os.path.splitext(file_path)
    count = 1
    while os.path.exists(file_path):
        file_path = filename + "_" + str(count) + extension
        count += 1
    return file_path


class InventoryMissingHostError(Exception):
    """
    Exception raised if the inventory file has been found to be missing
    a hostname that it should have.
    """
    pass


def gen_inventory_with_altered_vars(hosts, disallowed_vars, new_vars):
    """
    Generates a new inventory file based on the actual one after
    modifying the host variables.

    :param hosts: A list of hostnames that will have their variables
        edited
    :type hosts: list of strs
    :param disallowed_vars: A list of variables that will be removed
        from all of the lines in the inventory
    :type disallowed_vars: list of strs
    :param new_vars: A list of variables that will be added to the
        lines for any of the provided hostnames
    :type new_vars: list of strs
    :raises InventoryMissingHostError: If the inventory is missing any
        of the provided hostnames
    :return: The path to the newly generated inventory file
    :rtype: str
    """
    # Read the lines from the inventory file into inventory_lines
    with open(defs.AnsiblePaths.inventory_file, 'r') as file_stream:
        inventory_lines = file_stream.readlines()
    # All hostnames are initially not found in the inventory
    found_host_lines = {hostname: False for hostname in hosts}
    # Iterate through the lines of the inventory
    for index, line in enumerate(inventory_lines):
        # Split the line into a list by whitespace
        line_elements = line.split()
        if line_elements:
            # is_known_host_line will store if this line contains
            # one of the provided hostnames at the start of it
            is_known_host_line = False
            if line_elements[0] in hosts:
                is_known_host_line = True
                # Remember that we've found this host in the inventory
                found_host_lines[line_elements[0]] = True
                # Skip the first element (the machine name) -
                # don't want to risk removing it
                elements_to_search = line_elements[1:]
            else:
                elements_to_search = line_elements
            # Remove any disallowed variables from the line
            for elem in elements_to_search:
                for var in disallowed_vars:
                    if elem.startswith(var):
                        line_elements.remove(elem)
                        break
            if is_known_host_line:
                # Add the provided new variables to the line
                line_elements += new_vars
            # Store the updated line
            inventory_lines[index] = ' '.join(line_elements) + '\n'
    # Check if there were any hosts that were provided that we didn't
    # find in the inventory and raise an error if there were
    for hostname, found in found_host_lines.items():
        if not found:
            error_msg = (
                f"{colours.FAIL}[ERROR] Could not find an entry for "
                f"{hostname} in the inventory file. Please check "
                f"the inventory ({defs.AnsiblePaths.inventory_file}) and "
                f"try again.{colours.CLEAR}")
            raise InventoryMissingHostError(error_msg)
    # Generate and return the path to the new inventory file
    generated_inventory_file = get_unique_file_path(
        defs.AnsiblePaths.inventory_file)
    with open(generated_inventory_file, 'w') as file_stream:
        file_stream.writelines(inventory_lines)
    return generated_inventory_file


class CircularRoleDependenciesError(Exception):
    """
    Exception raised when circular dependencies are detected when
    trying to retrieve dependencies for an Ansible role.
    """
    pass


def _warn_invalid_role_entry(role):
    """
    Prints a warning message, informing the user of an entry in a
    list of roles that couldn't be parsed.

    :param role: The entry's contents to turn into a string in the
        output
    :type role: any type that can be converted to str
    """
    print(
        f"{colours.WARNING}[WARNING] Couldn't parse object "
        f"in role list: {str(role)}. You may wish to "
        f"check this.{colours.CLEAR}"
    )


def parse_role_list(role_list):
    """
    Parse and return the names of the roles from the provided role list
    (that comes from an Ansible play or role dependencies file).

    For any entries in the list that couldn't be parsed as a reference
    to a role, a warning message will be printed out.

    :param role_list: A list of role objects to be parsed, in the
        format that Ansible expects
    :type role_list: list of objects
    :return: A list of the found role names in order
    :rtype: list of strs
    """
    found_roles = []
    name_key = 'name'
    role_key = 'role'
    for object in role_list:
        if isinstance(object, str):
            found_roles.append(object)
        elif isinstance(object, dict):
            if name_key in object:
                key = name_key
            elif role_key in object:
                key = role_key
            else:
                key = None
            if key:
                if isinstance(object[key], str):
                    found_roles.append(object[key])
                else:
                    _warn_invalid_role_entry(object[key])
            else:
                _warn_invalid_role_entry(object)
        else:
            _warn_invalid_role_entry(object)
    return found_roles


def get_role_dependencies(role, quiet_output=False):
    """
    Inspects the meta main YAML file of the provided role to find the
    latter's dependencies.

    :param role: The role to retrieve dependencies for. For example,
        'gitlab-runner'.
    :type role: str
    :param quiet_output: If True, suppress all output apart from
        warnings of invalid dependency syntax, defaults to False
    :type quiet_output: bool, optional
    :raises CircularRoleDependenciesError: If any circular role
        dependencies are detected
    :return: A set of strings, with each string giving the name of
        the dependency (Ansible role). The set will be empty if no
        dependencies were found.
    :rtype: set
    """
    # Get the path to the main.yml file in the meta subdirectory
    # for the role
    role_meta_main_file = os.path.join(
        defs.AnsiblePaths.roles_directory,
        role,
        defs.AnsiblePaths.roles_meta_main_file
    )
    if not os.path.exists(role_meta_main_file):
        # If the meta main.yml file doesn't exist, this role
        # has no dependencies
        return set()

    found_dependencies = set()
    meta_contents = load_yaml_file(role_meta_main_file)
    dependencies_key = 'dependencies'
    if (dependencies_key in meta_contents and
            isinstance(meta_contents[dependencies_key], list)):
        found_dependencies = set(parse_role_list(
            meta_contents[dependencies_key]))
    if found_dependencies:
        for i in found_dependencies:
            # Find dependencies of the dependencies
            try:
                sub_dependencies = get_role_dependencies(
                    role=i, quiet_output=True)
            except RecursionError:
                err_msg = ('You appear to have circular dependencies '
                           'present in your role definitions. Please '
                           'check the dependencies carefully, fix the '
                           'problem, and try again.')
                raise CircularRoleDependenciesError(err_msg)
            found_dependencies = found_dependencies.union(
                sub_dependencies)
        if not quiet_output:
            print(
                f"[INFO] Found role dependencies for {role}: "
                f"{', '.join(found_dependencies)}"
            )
    return found_dependencies


def is_git_repository(is_git_repository_override=None):
    """
    Determines if the codebase is a git repository by checking if the
    .git directory exists.

    :param is_git_repository_override: If set, use this in place of the
        actual git repository check result, defaults to None
    :type is_git_repository_override: bool, optional
    :return: True if the .git directory exists, False otherwise
    :rtype: bool
    """
    if is_git_repository_override is not None:
        return is_git_repository_override
    git_directory = os.path.join(defs.REPOSITORY_ROOT_DIR, '.git')
    return os.path.exists(git_directory) and os.path.isdir(git_directory)


def is_repository_clean(testing_output=None):
    """
    Determines if the repository is clean or dirty via the git status
    --porcelain command.

    :param testing_output: If set, use this in place of the actual
        output from the git command, defaults to None
    :type testing_output: str, optional
    :return: True if the repository is clean, False if it's dirty
    :rtype: bool
    """
    git_cmd_result = subprocess.run(
        ['git', 'status', '--porcelain'],
        stdout=subprocess.PIPE,
        check=True
    )
    if testing_output == '':
        return True
    if testing_output or git_cmd_result.stdout.decode().strip():
        print('The repository contains local modifications')
        return False
    return True


def is_unpushed_commits(testing_output=None):
    """
    Determines if any of the local branches of the repository have
    commits that have not yet been pushed to the remote.

    :param testing_output: If set, use this in place of the actual
        output from the git command, defaults to None
    :type testing_output: str, optional
    :return: True if there are unpushed commits, False if there are not
    :rtype: bool
    """
    # Fetches all the local branch names
    git_branches_cmd_result = subprocess.run(
        ['git', 'for-each-ref', '--format', '%(refname:short)',
         'refs/heads/'],
        stdout=subprocess.PIPE,
        check=True
    )
    result = False
    branches = git_branches_cmd_result.stdout.decode().splitlines()
    for branch in branches:
        git_cmd_result = subprocess.run(
            ['git', 'log', branch, '--not', '--remotes'],
            stdout=subprocess.PIPE,
            check=True
        )
        if testing_output == '':
            return True
        if testing_output or git_cmd_result.stdout.decode().strip():
            print(
                f"The repository contains unpushed commits on the '{branch}' "
                'branch'
            )
            result = True
    return result


class FailedGitChecks(Exception):
    """
    Exception raised if the check_for_git_issues() function finds
    issues.
    """
    pass


def check_for_git_issues(is_clean_testing_output=None,
                         is_unpushed_testing_output=None,
                         is_git_repository_override=None):
    """
    If the codebase is a git repository, performs these checks:
    Ensures that the repository is clean, and
    Ensures that there are no unpushed commits to the main branch.

    :param is_clean_testing_output: If set, use this in place of the
        actual git output for the repository clean check, defaults to
        None
    :type is_clean_testing_output: str, optional
    :param is_unpushed_testing_output: If set, use this in place of the
        actual git output for the unpushed commits check, defaults to
        None
    :type is_unpushed_testing_output: str, optional
    :param is_git_repository_override: If set, use this in place of the
        actual git repository check result, defaults to None
    :type is_git_repository_override: bool, optional
    :raises FailedGitChecks: If the checks fail
    """
    if is_git_repository(is_git_repository_override):
        is_clean = is_repository_clean(is_clean_testing_output)
        is_unpushed = is_unpushed_commits(is_unpushed_testing_output)
        if not is_clean or is_unpushed:
            raise FailedGitChecks(
                'Git checks failed. Please check the above error message(s) '
                'and try again.'
            )


def get_current_git_commit_sha():
    """
    Retrieves the git commit SHA for the current commit of this
    repository via the "git rev-parse HEAD" command.

    :return: The git commit SHA for the current commit
    :rtype: str
    """
    git_cmd_result = subprocess.run(['git', 'rev-parse', 'HEAD'], check=True,
                                    stdout=subprocess.PIPE)
    return git_cmd_result.stdout.decode().strip()


def run_playbook(path, local=True, extra_vars=None, inventory_path=None,
                 new_login=False, limit=None, user=None, become_method=None,
                 check=False, diff=False, force_venv_python=False,
                 verbose=False, print_command_output=True,
                 ask_become_pass=False, ask_ssh_pass=False):
    """
    Run an Ansible playbook with the ansible-playbook command.

    :param path: The path to the playbook to run
    :type path: str
    :param local: If True, use connection=local when running the
        playbook, defaults to True
    :type local: bool, optional
    :param extra_vars: Extra variables to pass over the command line;
        can be a list or single string value, defaults to None
    :type extra_vars: str or list, optional
    :param inventory_path: Path to the inventory file, if None, just
        use localhost, defaults to None
    :type inventory_path: str, optional
    :param new_login: Run the command in a new login shell, defaults to
        False
    :type new_login: bool, optional
    :param limit: For limiting the playbook to particular hosts /
        groups with the --limit argument, defaults to None
    :type limit: str, optional
    :param user: For setting the user to connect as, defaults to None
    :type user: str, optional
    :param become_method: For setting a custom become method e.g. su,
        defaults to None
    :type become_method: str, optional
    :param check: If True, use check mode, defaults to False
    :type check: bool, optional
    :param diff: If True, use diff mode, defaults to False
    :type diff: bool, optional
    :param force_venv_python: If True, set the
        ansible_python_interpreter Ansible variable to the path to the
        Python binary that is present in the repo's venv/bin directory
        (this assumes that the user has run the iac_deployer script to
        create the virtualenv). This will force Ansible to use the
        Python interpreter at this path FOR ALL HOSTS in the inventory.
        Defaults to False.
    :type force_venv_python: bool, optional
    :param verbose: Increase output verbosity, defaults to False
    :type verbose: bool, optional
    :param print_command_output: If True, print the Ansible command
        output, defaults to True
    :type print_command_output: bool, optional
    :param ask_become_pass: If True, prompt the user for the Ansible
        become password for privilege escalation, defaults to False
    :type ask_become_pass: bool, optional
    :param ask_ssh_pass: If True, force Ansible to prompt the user for
        SSH passwords when connecting to the target hosts, defaults
        to False
    :type ask_ssh_pass: bool, optional
    :raises TypeError: If extra_vars is the wrong type
    :raises subprocess.CalledProcessError: If the ansible-playbook
        command reports an error.
    :return: The stdout, stderr, and stdout & stderr combined output,
        as a tuple
    :rtype: tuple containing strs
    """
    # TODO rename inventory_path to just inventory
    cmd = [defs.ANSIBLE_PLAYBOOK_CMD, path]
    cmd.append('-i')
    if inventory_path:
        inventory_temp_file = None
        cmd.append(inventory_path)
    else:
        # Generate the inventory file
        inventory_temp_file = tempfile.NamedTemporaryFile()
        inventory_temp_file.write(
            'localhost ansible_python_interpreter='
            f"{defs.PythonPaths.VENV_PYTHON_BINARY}\n".encode())
        inventory_temp_file.flush()
        cmd.append(inventory_temp_file.name)
    if force_venv_python:
        cmd.append('-e')
        cmd.append('ansible_python_interpreter='
                   f"{defs.PythonPaths.VENV_PYTHON_BINARY}")
    if local:
        cmd.append('--connection=local')
    if limit:
        cmd += ['--limit', limit]
    if user:
        cmd += ['--user', user]
    if become_method:
        cmd.append(f"--become-method={become_method}")
    if check:
        cmd.append('--check')
    if diff:
        cmd.append('--diff')
    if ask_become_pass:
        cmd.append('--ask-become-pass')
    if ask_ssh_pass:
        cmd.append('--ask-pass')
    if extra_vars:
        if isinstance(extra_vars, list):
            for var in extra_vars:
                assert isinstance(var, str)
                cmd.append('-e')
                cmd.append(var)
        elif isinstance(extra_vars, str):
            cmd.append('-e')
            cmd.append(extra_vars)
        else:
            raise TypeError(f"extra_vars must be str or list, got "
                            f"{type(extra_vars)}")
    # Force colour output even though the process is detached from a TTY
    my_env = os.environ.copy()
    my_env['ANSIBLE_FORCE_COLOR'] = 'true'
    try:
        stdout, stderr, combined = proc.execute(
            cmd, verbose=print_command_output, new_login=new_login,
            print_command_when_verbose=verbose, env=my_env)
    finally:
        if inventory_temp_file:
            inventory_temp_file.close()
    return stdout, stderr, combined


def concatenate_playbooks(machine_name, final_playbook_name='site.yml',
                          verbose=False, quiet=False,
                          custom_root_ansible_dir=None):
    """
    Concatenates the playbook for the provided machine and the
    playbook(s) for the machine's group(s) (if any) together into one
    playbook that can then be run with Ansible.

    Uses Ansible to parse the inventory file and generate JSON output
    that shows the structure of it, which is used to determine what
    groups the machine is in.

    Then, the playbooks for the machine and groups are syntax checked,
    again using Ansible.

    Finally, the playbooks' YAML contents are parsed, combined together
    into one, and then that is written to the output file.

    :param machine_name: The name of the machine (used to decide which
        host playbook and which group playbooks to include)
    :type machine_name: str
    :param final_playbook_name: The name of the generated playbook,
        defaults to 'site.yml'
    :type final_playbook_name: str, optional
    :param verbose: If True, enable verbose mode for running the
        Ansible commands, defaults to False
    :type verbose: bool, optional
    :param quiet: If True, suppress output apart from errors, defaults
        to False
    :type quiet: bool, optional
    :param custom_root_ansible_dir: If specified, use this path as the
        root Ansible directory instead of the default i.e. this
        directory should contain the hosts and groups directories.
        Defaults to None
    :type custom_root_ansible_dir: str, optional
    :raises subprocess.CalledProcessError: If the Ansible commands
        reported any errors
    :raises SyntaxError: If the host and / or group playbooks contain
        any syntax errors, as determined by the Ansible syntax check
    :raises Exception: If any other errors occur
    :return: The full path to the generated playbook file. Its name may
        be different to final_playbook_name if a file already existed
        at that path
    :rtype: str
    """
    # Determine generated playbook file as well as hosts and groups
    # directory locations
    if custom_root_ansible_dir:
        playbook_dirs = {
            'hosts': os.path.join(custom_root_ansible_dir, 'hosts/'),
            'groups': os.path.join(custom_root_ansible_dir, 'groups/')
        }
    else:
        playbook_dirs = {
            'hosts': defs.AnsiblePaths.host_playbooks_directory,
            'groups': defs.AnsiblePaths.group_playbooks_directory
        }
    final_playbook_path = os.path.join(playbook_dirs['hosts'],
                                       final_playbook_name)

    try:
        # Use the ansible command to generate a list of groups with
        # their hosts in JSON format from the inventory
        stdout, _, _ = proc.execute([
            defs.ANSIBLE_CMD,
            '-i',
            defs.AnsiblePaths.inventory_file,
            '-m',
            'debug',
            '-a',
            'var=groups',
            'localhost'], verbose)
    except subprocess.CalledProcessError as err:
        print(f"{colours.FAIL}Could not retrieve the list of groups from the "
              'inventory file. Please check it and try again.'
              f"{colours.CLEAR}")
        raise err

    match = re.search(r'{(\s|\S)*}', stdout)
    if not match:
        raise subprocess.CalledProcessError(
            'Ansible command for listing groups did not provide correct '
            f"format for JSON parsing, its output was:\n{stdout}")
    stdout_just_json = match.group(0)
    try:
        inventory_groups = json.loads(stdout_just_json)
    except Exception as err:
        print('Couldn\'t parse Ansible group output as JSON! Error details:')
        raise err

    # Will contain the machine specific playbook, plus the playbooks
    # for all the groups the machine is a part of
    playbooks_to_include = [get_directory_yaml_file(playbook_dirs['hosts'],
                                                    machine_name)]

    for group_name, hosts_list in inventory_groups['groups'].items():
        if isinstance(hosts_list, list) and machine_name in hosts_list:
            try:
                group_playbook_path = get_directory_yaml_file(
                    playbook_dirs['groups'], group_name)
            except FileNotFoundError:
                if group_name != 'all' and group_name != 'ungrouped':
                    print(f"{colours.WARNING}[WARNING] Could not find "
                          f"playbook for group '{group_name}' in directory "
                          f"{playbook_dirs['groups']}{colours.CLEAR}")
                continue
            playbooks_to_include.append(group_playbook_path)

    final_playbook_yaml = []
    for playbook_path in playbooks_to_include:
        playbook_name = os.path.basename(playbook_path)
        if not quiet:
            print(f"Syntax check {str(playbook_name)}")
        try:
            proc.execute([
                defs.ANSIBLE_PLAYBOOK_CMD,
                '-i',
                defs.AnsiblePaths.inventory_file,
                '--syntax-check',
                playbook_path], verbose)
        except subprocess.CalledProcessError:
            raise SyntaxError(f"Syntax check for '{playbook_name}' playbook "
                              'failed! Please check it and try again.')
        try:
            with open(playbook_path) as file_stream:
                loaded_yaml = yaml.safe_load(file_stream)
                final_playbook_yaml += loaded_yaml
        except yaml.parser.ParserError as err:
            print(f"{colours.FAIL}Invalid YAML! Please check "
                  f"{str(playbook_path)} and try again. Error details:"
                  f"{colours.CLEAR}")
            raise err

    actual_playbook_path = get_unique_file_path(final_playbook_path)
    try:
        with open(actual_playbook_path, 'w') as file_stream:
            yaml.dump(final_playbook_yaml, file_stream)
    except Exception as err:
        if os.path.isfile(actual_playbook_path):
            os.remove(actual_playbook_path)
        raise err
    return actual_playbook_path
