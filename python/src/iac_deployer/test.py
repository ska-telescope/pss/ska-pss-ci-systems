import docker
import uuid

from . import colours
from . import definitions as defs
from . import utils


class Test:
    """
    Class representing an abstract test.
    """

    def __init__(self, verbose=False, force_docker_install=False,
                 no_docker_sudo_prompt=False, pause_before_teardown=False):
        """
        Constructor for the test.

        :param force_docker_install: If True, don't prompt the user if
            docker needs installing - just install it. Defaults to
            False
        :type force_docker_install: bool, optional
        :param no_docker_sudo_prompt: If True, don't prompt the user
            for a sudo password when running the Docker install
            playbook. Useful for unit testing. Defaults to False
        :type no_docker_sudo_prompt: bool, optional
        :param pause_before_teardown: If True, wait for user input
            before tearing down the test resources. Useful for
            debugging. Defaults to False
        :type pause_before_teardown: bool, optional
        """
        self.verbose = verbose
        self.force_docker_install = force_docker_install
        self.no_docker_sudo_prompt = no_docker_sudo_prompt
        self.pause_before_teardown = pause_before_teardown

        # Generate a unique identifier for this test. Can be used to,
        # for example, avoid Docker container name collisions.
        self.uuid = str(uuid.uuid4())

        # For using the Docker API
        self.docker_client = docker.from_env()

        # Name of the Docker network to add containers to during the test
        self.docker_network_name = (defs.TEST_DOCKER_NETWORK_VALUE_PREFIX +
                                    self.uuid)
        self.docker_network_obj = None

        # Create the Docker network for the test
        self.docker_network_obj = self.docker_client.networks.create(
            self.docker_network_name, driver='bridge')

    def run(self, docker_install_override=None,
            final_docker_install_override=None,
            docker_permission_override=None):
        """
        Runs the test. Ensures docker is installed before beginning the
        test. Always calls _teardown_test() if the test was attempted.
        Prints a success or failure message depending on the result of
        the test.

        :param docker_install_override: Sent to corresponding variable
            for utils.ensure_docker(). For unit testing. Defaults to
            None
        :type docker_install_override: bool, optional
        :param final_docker_install_override: Sent to corresponding
            variable for utils.ensure_docker(). For unit testing.
            Defaults to None
        :type final_docker_install_override: bool, optional
        :param docker_permission_override: Sent to corresponding
            variable for utils.ensure_docker(). For unit testing.
            Defaults to None
        :type docker_permission_override: bool, optional
        :raises err: Exception, if any exceptions occur during the test
        :return: The result of the test; True for success, False for
            skipped or failure
        :rtype: bool
        """
        # Ensure docker is installed if we are running macOS
        if not utils.ensure_docker_mac_os():
            return self._fail()

        # For other OSes, install Docker if it isn't found
        try:
            docker_install_result = utils.ensure_docker(
                force=self.force_docker_install,
                no_sudo_prompt=self.no_docker_sudo_prompt,
                is_installed_override=docker_install_override,
                final_is_installed_override=final_docker_install_override,
                permission_check_override=docker_permission_override
            )
        except utils.DockerPermissionError as err:
            print(err)
            return self._skip()
        if not docker_install_result:
            return self._skip()

        if not utils.check_docker_permissions(docker_permission_override):
            utils.print_docker_permissions_error()
            return self._skip()

        try:
            res = self._execute_test()
        except Exception as err:
            self._fail()
            raise err
        finally:
            if self.pause_before_teardown:
                input('Press Enter to perform test teardown...')
            self._teardown_test()
            # Remove the Docker network if it was created
            if self.docker_network_obj:
                self.docker_network_obj.remove()

        if res:
            return self._success()
        return self._fail()

    def _success(self):
        """
        Signify test success; print a success message and return True.

        :return: True
        :rtype: bool
        """
        print(f"{colours.SUCCESS}Test SUCCESS{colours.CLEAR}")
        return True

    def _fail(self):
        """
        Signify test failure; print a failure message and return False.

        :return: False
        :rtype: bool
        """
        print(f"{colours.FAIL}Test FAILED!{colours.CLEAR}")
        return False

    def _skip(self):
        """
        Signify that the test has been skipped; print a "skipped"
        message and return False.

        :return: False
        :rtype: bool
        """
        print(f"{colours.WARNING}Test SKIPPED{colours.CLEAR}")
        return False

    def _execute_test(self):
        """
        To override
        """
        raise NotImplementedError()

    def _teardown_test(self):
        """
        To override
        """
        raise NotImplementedError()
