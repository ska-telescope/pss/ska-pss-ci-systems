import datetime
import json
import os
import re
import subprocess
import textwrap

from . import colours
from . import definitions as defs
from . import machine
from . import utils


STATUS_REPO_DIR = os.path.join(defs.REPOSITORY_ROOT_DIR, 'status-repository')


def print_machines_status(machine_list, detailed=False):
    """
    Prints the status of each machine from machine_list.

    :param machine_list: The list of machines to print status for
    :type machine_list: list of strs
    :param detailed: If True, show detailed machine status output,
        defaults to False
    :type detailed: bool, optional
    :raises utils.UnknownMachineNameError: If a machine name exists in
        the list that doesn't have a playbook in the ansible/hosts
        directory.
    :raises utils.FailedGitChecks: If the common git checks fail
    """
    utils.check_for_git_issues()
    all_machines = machine.get_machine_names()

    if not machine_list:
        machine_list = all_machines

    for machine_name in machine_list:
        if machine_name not in all_machines:
            raise utils.UnknownMachineNameError(machine_name)
        status = MachineStatus(machine_name)
        status.print_user_friendly_status(detailed=detailed)


class StatusValues:
    """
    Stores constants for the different possible status messages that a
    machine can have.
    """
    NOT_DEPLOYED = 'not_deployed'
    FAILED_ROLLBACK = 'failed_rollback'
    FAILED_DEPLOYMENT = 'failed_deployment'
    DEPLOYED = 'deployed'


class GitCommandError(Exception):
    """
    Exception raised if an error occurs when running a git command.
    """
    pass


class MachineStatus:
    """
    Class representing the status of a machine, based on the contents
    of the status git repository.
    """

    class Constants:
        """
        Stores general constants in relation to machine status.
        """
        COMMIT_SHA_KEY = 'commit_sha'
        STATUS_KEY = 'status'

    def __init__(self, machine_name, status_repo_url=None, quiet=False):
        """
        Constructor for the MachineStatus.

        :param machine_name: The name of the machine to track the
            status of
        :type machine_name: str
        :param status_repo_url: If not None, use this URL for the
            status repository rather than the default, defaults to None
        :type status_repo_url: str, optional
        :param quiet: If True, only print Ansible output and error
            messages, defaults to False
        :type quiet: bool, optional
        """
        self._machine_name = machine_name
        self._quiet = quiet
        # If we are unit testing, set _local_only to True
        self._local_only = defs.UNIT_TESTS
        self._machine_directory_path = os.path.join(STATUS_REPO_DIR,
                                                    self._machine_name)
        self._status_json_path = os.path.join(self._machine_directory_path,
                                              'status.json')

        self._repo_url = (
            'git@gitlab.com:ska-telescope/pss/ska-pss-ci-systems-status.git'
        )
        if status_repo_url:
            self._repo_url = status_repo_url

        if not (os.path.exists(STATUS_REPO_DIR) and
                os.path.exists(os.path.join(STATUS_REPO_DIR, '.git'))):
            if not self._local_only:
                self.clone_repository()
        else:
            if not self._local_only:
                self.pull_repository()

        self.reload()

    def _print(self, msg):
        """
        Prints a message if not in quiet mode.

        :param msg: The message to print
        :type msg: str
        """
        if not self._quiet:
            print(msg)

    def _run_git_cmd(self, cmd, change_to_status_dir=True):
        """
        Runs the provided git command after optionally changing
        directories to the status repository's directory.

        :param cmd: The command to run. It should be a git command
        :type cmd: list of strs
        :param change_to_status_dir: If True, the command will be run
            after changing directories to the status repository's
            directory. The directory will be changed back to the
            standard repository's directory afterwards. Defaults to
            True
        :type change_to_status_dir: bool, optional
        :raises GitCommandError: If an error occurred when running the
            git command
        :return: The contents of stdout, captured from the git subprocess
        :rtype: str
        """
        try:
            if change_to_status_dir:
                os.chdir(STATUS_REPO_DIR)
            git_cmd_result = subprocess.run(
                cmd,
                check=True,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE
            )
        except subprocess.CalledProcessError as err:
            raise GitCommandError(
                f"Error running git command {err.cmd} for the status "
                f"repository. Error details:\n{err.stderr.decode()}"
            )
        finally:
            if change_to_status_dir:
                os.chdir(defs.REPOSITORY_ROOT_DIR)
        return git_cmd_result.stdout.decode()

    def clone_repository(self):
        """
        Runs the git clone command to clone the status repository
        from the defined URL.

        :raises Exception: If local_only was set to True during object
            construction
        """
        if self._local_only:
            raise Exception(
                'Trying to clone status repository when local only is set!'
            )
        self._print('[STAGE] Cloning status repository')
        self._run_git_cmd(
            ['git', 'clone', self._repo_url, STATUS_REPO_DIR],
            change_to_status_dir=False
        )

    def pull_repository(self):
        """
        Runs the git pull command to update the status repository
        from the remote.

        :raises Exception: If local_only was set to True during object
            construction
        """
        if self._local_only:
            raise Exception(
                'Trying to run git pull for status repository when local '
                'only is set!'
            )
        self._print('[STAGE] Updating status repository from remote')
        self._run_git_cmd(['git', 'pull'])

    def push_repository(self):
        """
        Runs the git push command to push local commits from the status
        repository to the remote.

        :raises Exception: If local_only was set to True during object
            construction
        """
        if self._local_only:
            raise Exception(
                'Trying to run git push for status repository when local '
                'only is set!'
            )
        self._print('[STAGE] Pushing status repository updates to remote')
        self._run_git_cmd(['git', 'push'])

    def reload(self):
        """
        Reads the values from the appropriate status.json file from the
        status repository. If the status.json file doesn't exist yet,
        it is created.
        """
        # If the status.json file doesn't exist yet, just write initial
        # values to it
        if not os.path.exists(self._status_json_path):
            self.write(None, StatusValues.NOT_DEPLOYED)
            return

        # Otherwise, read the values from status.json
        with open(self._status_json_path, 'r') as file_stream:
            status_obj = json.load(file_stream)
        self._commit_sha = status_obj[MachineStatus.Constants.COMMIT_SHA_KEY]
        self._status = status_obj[MachineStatus.Constants.STATUS_KEY]

    def get_commit_sha(self):
        """
        Returns the value of the commit SHA.

        :return: The commit SHA
        :rtype: str
        """
        return self._commit_sha

    def get_status(self):
        """
        Returns the value of the status message i.e. either "deployed",
        "not_deployed", "failed_deployment", or "failed_rollback".

        :return: The status message
        :rtype: str
        """
        return self._status

    def print_user_friendly_status(self, detailed=False):
        """
        Prints a user-friendly status message for the machine.

        :param detailed: If True, when showing the git difference
            between the machine's current status and the current
            HEAD, show full file contents rather than a summary of
            changed filenames. Defaults to False
        :type detailed: bool, optional
        """
        print(f"[STAGE] Show status of machine '{self._machine_name}'")
        if self._status == StatusValues.NOT_DEPLOYED:
            print(
                f"Machine '{self._machine_name}' is ready to be deployed to, "
                'since nothing is currently deployed to it.'
            )
        elif self._status == StatusValues.FAILED_ROLLBACK:
            msg = (
                f"The last rollback of machine '{self._machine_name}' "
                'failed. Please check the logs to see what the error was '
                'and, after making any commits or changes on the machine to '
                'fix the error, try again. '
                'Please note that any new rollbacks you make '
                'will be initiated for the HEAD commit, not for the '
                'commit that originally failed.'
            )
            print(textwrap.fill(msg))
            print('')
            print(f"Failed commit SHA: '{self._commit_sha}'")
        elif self._status == StatusValues.FAILED_DEPLOYMENT:
            msg = (
                f"The last deployment to machine '{self._machine_name}' "
                'failed. Please check the logs to see what the error was '
                'and, after making any commits or changes on the machine to '
                'fix the error, try again. '
                'Please note that any new deployments you make '
                'will be initiated for the HEAD commit, not for the '
                'commit that originally failed.'
            )
            print(textwrap.fill(msg))
            print('')
            print(f"Failed commit SHA: '{self._commit_sha}'")
        elif self._status == StatusValues.DEPLOYED:
            msg = (
                f"Machine '{self._machine_name}' is currently deployed to. "
                'See the commit SHA for the successful deployment below.'
            )
            print(textwrap.fill(msg))
            print(f"\nCommit SHA: '{self._commit_sha}'")

        machine_playbook_path = utils.get_directory_yaml_file(
            defs.AnsiblePaths.host_playbooks_directory,
            self._machine_name)
        _, machine_components = machine.parse_machine_playbook(
            machine_playbook_path, self._machine_name, quiet=True)
        machine_components = [
            os.path.join(defs.AnsiblePaths.roles_directory, component)
            for component in machine_components
        ]
        base_command = ['git', 'diff', '--color']
        if not detailed:
            base_command.append('--name-status')
        if self._commit_sha:
            base_command += [self._commit_sha]
        else:
            # Find the first revision SHA
            rev_list_result = self._run_git_cmd(['git', 'rev-list', 'HEAD'],
                                                change_to_status_dir=False)
            base_command.append(rev_list_result.splitlines()[-1])
        base_command += ['HEAD', '--', machine_playbook_path]
        output = self._run_git_cmd(base_command + machine_components,
                                   change_to_status_dir=False)
        print('')
        if output:
            msg = (
                'The file changes below show the difference between the '
                'machine\'s current status and the current git HEAD of the '
                'repository. Note that the changes shown are restricted to '
                'the machine\'s playbook and its Ansible roles (components). '
            )
            if not detailed:
                msg += (
                    'Also, you may wish to view more detailed file changes '
                    'by using the --detailed argument.'
                )
            print(textwrap.fill(msg) + '\n')
            print(output, end='')
        elif self._status == StatusValues.DEPLOYED:
            print(
                f"{colours.SUCCESS}The machine's status is up-to-date with "
                f"the git HEAD of the repository{colours.CLEAR}"
            )

    def git_commit(self, message):
        """
        Runs the git commit command with the provided message.

        :param message: The commit message
        :type message: str
        """
        self._run_git_cmd(['git', 'commit', '-m', message])

    def git_add(self, path):
        """
        Runs the git add command with the provided path.

        :param path: The path to the file to add
        :type path: str
        """
        self._run_git_cmd(['git', 'add', path])

    @staticmethod
    def get_ansible_log_timestamp():
        """
        Generates a timestamp based on the current time in the correct
        format for the filename of an Ansible output log file.

        :return: The generated timestamp
        :rtype: str
        """
        return datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S")

    def write(self, commit_sha, status_message, ansible_log=None,
              ansible_log_commit_sha=None, ansible_log_timestamp=None,
              remove_ansible_log_colour_codes=True):
        """
        Writes an update to the machine's status to the status.json
        file, optionally creates a log file containing the provided
        Ansible output, and then commits and pushes the changes to the
        status repository.

        :param commit_sha: The commit SHA to store in status.json
        :type commit_sha: str
        :param status_message: The status message to store in
            status.json
        :type status_message: str
        :param ansible_log: The Ansible output to store in the created
            log file, defaults to None
        :type ansible_log: str, optional
        :param ansible_log_commit_sha: If specified, use this commit
            SHA in the Ansible log filename rather than commit_sha,
            defaults to None
        :type ansible_log_commit_sha: str, optional
        :param ansible_log_timestamp: If specified, use this timestamp
            in the filename of the generated log file storing the
            Ansible output. If not specified, the time that the log
            file is being written to will be used. Defaults to None
        :type ansible_log_timestamp: str, optional
        :param remove_ansible_log_colour_codes: If True, remove any
            ANSI colour code escape sequences from the provided
            Ansible log via a regular expression. Defaults to True
        :type remove_ansible_log_colour_codes: bool, optional
        """
        self._commit_sha = commit_sha
        self._status = status_message

        status_obj = {
            MachineStatus.Constants.COMMIT_SHA_KEY: commit_sha,
            MachineStatus.Constants.STATUS_KEY: status_message
        }

        # Ensure the machine directory exists
        os.makedirs(os.path.dirname(self._status_json_path), exist_ok=True)
        # Write to status.json in the machine directory
        with open(self._status_json_path, 'w') as file_stream:
            file_stream.write(json.dumps(status_obj) + '\n')
        # git add status.json
        self.git_add(self._status_json_path)

        if ansible_log:
            if remove_ansible_log_colour_codes:
                ansible_log = re.sub(r'\x1b\[[0-9;]*m', '', ansible_log)

            if (status_message == StatusValues.NOT_DEPLOYED or
                    status_message == StatusValues.FAILED_ROLLBACK):
                mode = 'rollback'
            else:
                mode = 'deploy'

            if not ansible_log_timestamp:
                ansible_log_timestamp = (
                    MachineStatus.get_ansible_log_timestamp())

            if ansible_log_commit_sha:
                log_filename = (
                    f"{ansible_log_timestamp}-{mode}-{ansible_log_commit_sha}"
                    ".log"
                )
            else:
                log_filename = (
                    f"{ansible_log_timestamp}-{mode}-{commit_sha}.log"
                )

            log_file_path = os.path.join(self._machine_directory_path,
                                         log_filename)
            self._print(f"Writing log to {log_filename}")
            with open(log_file_path, 'w') as file_stream:
                file_stream.write(ansible_log)
            self.git_add(log_file_path)

        commit_message = (
            f"Update '{self._machine_name}' machine status\n"
        )
        commit_message += (
            f"- Set commit SHA to '{commit_sha}'\n" if commit_sha else
            '- Clear commit SHA\n'
        )
        commit_message += (
            f"- Set machine status message to '{status_message}'\n"
        )
        if ansible_log:
            commit_message += (
                f"- Add Ansible log file '{log_filename}'\n"
            )
        self.git_commit(commit_message)
        if not self._local_only:
            self.push_repository()

    def can_rollback(self):
        """
        Determines whether a rollback can currently be performed, based
        on the current status.

        Rollbacks can only be performed if the status is "deployed" or
        "failed_rollback" AND the commit SHA is not null.

        :return: True if a rollback can be performed, False otherwise
        :rtype: bool
        """
        return (
            self._status != StatusValues.NOT_DEPLOYED and
            self._status != StatusValues.FAILED_DEPLOYMENT and
            self._commit_sha
        )

    def can_deploy(self):
        """
        Determines whether a deployment can currently be performed,
        based on the current status.

        Deployments can only be performed if the status is
        "not_deployed" or "failed_deployment".

        :return: True if a deployment can be performed, False otherwise
        :rtype: bool
        """
        return (
            self._status == StatusValues.NOT_DEPLOYED or
            self._status == StatusValues.FAILED_DEPLOYMENT
        )
