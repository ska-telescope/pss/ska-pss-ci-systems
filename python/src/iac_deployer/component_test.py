import fnmatch
import os
import subprocess

from . import colours
from . import definitions as defs
from . import test
from . import utils


class MissingComponentTestsError(Exception):
    """
    Exception raised when there are one or more components that lack
    corresponding component tests.
    """
    pass


def check_component_tests():
    """
    Checks if the defined components (Ansible roles) have corresponding
    component tests, and, if any of them don't, raises any error.

    :raises MissingComponentTestsError: If there is at least one
        component that is missing a corresponding component test.
    """
    roles = set(get_component_roles())
    tests = set(get_component_tests())
    roles_without_tests = roles.difference(tests)
    if roles_without_tests:
        roles_without_tests = list(roles_without_tests)
        roles_without_tests.sort()
        error_msg = (
            f"The components {roles_without_tests} are missing corresponding "
            'component tests. Please create tests for them and try again.')
        import textwrap
        raise MissingComponentTestsError(textwrap.fill(error_msg))


def get_component_roles():
    """
    Gets the list of defined components (Ansible roles) from within the
    ansible/roles directory.

    :return: A sorted list of component role names
    :rtype: list of strs
    """
    result = []
    base_dir_path = defs.AnsiblePaths.roles_directory
    main_files = ['tasks/main.yml', 'tasks/main.yaml']
    for dir in os.listdir(base_dir_path):
        for main_file in main_files:
            test_path = os.path.join(dir, main_file)
            if os.path.isfile(os.path.join(base_dir_path, test_path)):
                result.append(dir)
                break
    result.sort()
    return result


def get_component_tests():
    """
    Gets all the directories in the component tests directory.

    :return: The list of directories
    :rtype: list of strs
    """
    result = []
    base_dir_path = defs.AnsiblePaths.component_tests_directory
    for dir in os.listdir(base_dir_path):
        current_test_dir = os.path.join(base_dir_path, dir)
        if os.path.isdir(current_test_dir):
            result.append(dir)
    result.sort()
    return result


def print_component_tests():
    """
    Prints out all the directories in the component tests directory
    """
    for dir in get_component_tests():
        print(dir)


class ComponentTest(test.Test):
    """
    Class defining a test for a particular component e.g. gitlab-runner
    """

    def __init__(self, component_name, os_list=None, verbose=False,
                 force_docker_install=False, no_docker_sudo_prompt=False,
                 test_docker_diff=None, pause_before_teardown=False):
        """
        Constructor for the component test

        :param component_name: The name of the component to test that
            should have a corresponding test playbook under ansible/
            tests/components/
        :type component_name: str
        :param os_list: List of operating systems to test the component
            on, defaults to None; in which case, all OSes will be used
        :type os_list: list, optional
        :param verbose: Increase output verbosity, defaults to False
        :type verbose: bool, optional
        :param force_docker_install: If True, don't prompt the user if
            docker needs installing - just install it. Defaults to
            False
        :type force_docker_install: bool, optional
        :param no_docker_sudo_prompt: If True, don't prompt the user
            for a sudo password when running the Docker install
            playbook. Useful for unit testing. Defaults to False
        :type no_docker_sudo_prompt: bool, optional
        :param test_docker_diff: For unit testing; to be used
            in place of the actual docker diff output for after each
            test playbook has been run, defaults to None
        :type test_docker_diff: list, optional
        :param pause_before_teardown: If True, wait for user input
            before tearing down the test resources. Useful for
            debugging. Defaults to False
        :type pause_before_teardown: bool, optional
        """
        test.Test.__init__(
            self, verbose=verbose, force_docker_install=force_docker_install,
            no_docker_sudo_prompt=no_docker_sudo_prompt,
            pause_before_teardown=pause_before_teardown)

        # Basic options
        self.component_name = component_name
        self.os_list = os_list

        # Store the base directory for the component test
        self.component_test_base_dir = os.path.join(
            defs.AnsiblePaths.component_tests_directory, self.component_name)

        # There should be a corresponding role directory for the
        # component
        self.component_role_directory = os.path.join(
            defs.AnsiblePaths.roles_directory,
            self.component_name
        )

        # Will store any role dependencies needed by the component
        # being tested
        self.component_dependencies = None

        # For unit testing
        self.test_docker_diff = test_docker_diff

    def _load_ignore_list(self, os_name=None, component_base_dir=None):
        """
        Loads and returns the contents of the ignore list file (either
        global or OS-specific).

        :param os_name: If specified, load the ignore list from the
            specified OS' subdirectory rather than the global ignore
            list, defaults to None
        :type os_name: str, optional
        :param component_base_dir: If specified, use this directory to
            look in for the ignore list, overriding the default
            behaviour of using this component's base directory to look
            in. Defaults to None
        :type component_base_dir: str, optional
        :raises TypeError: If the loaded ignore list is not in list
            format
        :return: The loaded ignore list, containing a set of file
            patterns to be ignored, or None if the ignore list could
            not be loaded
        :rtype: set
        """
        if not component_base_dir:
            component_base_dir = self.component_test_base_dir
        else:
            if not os.path.exists(component_base_dir):
                return set()

        if os_name:
            base_dir = os.path.join(component_base_dir, os_name)
        else:
            base_dir = component_base_dir

        # Attempt to find the ignore list file in base_dir
        try:
            path = utils.get_directory_yaml_file(
                base_dir,
                defs.AnsiblePaths.component_test_ignore_list)
        except FileNotFoundError:
            # The ignore list file doesn't exist
            return set()

        # Load the YAML from the file
        result = utils.load_yaml_file(path)
        if not isinstance(result, list):
            raise TypeError(f"The ignore list at {path} must contain a list "
                            'of file patterns. Please consult the README to '
                            'view the expected format.')
        for i in result:
            if not isinstance(i, str):
                raise TypeError(f"The ignore list at {path} must contain a "
                                'list of file patterns in string format. '
                                f"This is not a valid string: {str(i)}")
        return set(result)

    def _is_file_on_ignore_list(self, file_path, ignore_list):
        """
        Determines if a given file or directory path matches any of the
        patterns on the ignore list.

        :param file_path: The full path to the file or directory to
            check
        :type file_path: str
        :param ignore_list: The ignore list to check the file / dir
            against
        :type ignore_list: set
        :return: True if the file / dir matches any of the patterns on
            the ignore list according to Unix shell rules, False
            otherwise
        :rtype: bool
        """
        if ignore_list and isinstance(ignore_list, set):
            for pattern in ignore_list:
                if fnmatch.fnmatch(file_path, pattern):
                    return True
        return False

    def _generate_ignore_list(self, os_name=None):
        """
        Generates an ignore list of file patterns from this component
        test plus any dependent component tests. Can also be used to
        load OS-specific ignore lists for the component and
        dependencies.

        :param os_name: If specified, load OS-specific ignore lists for
            this OS rather than the global ignore lists, defaults to
            None
        :type os_name: str, optional
        :raises utils.CircularRoleDependenciesError: If any circular
            role dependencies are detected
        :return: The generated ignore list; the union of all found
            ignore lists that match the criteria
        :rtype: set
        """
        # Load the ignore list (either global or OS-specific) for ALL
        # components
        final_ignore_list = self._load_ignore_list(
            os_name=os_name,
            component_base_dir=defs.AnsiblePaths.global_ignore_lists_directory
        )

        # Load the ignore list (either global or OS-specific) for THIS
        # component and union it with the existing ignore list
        final_ignore_list = final_ignore_list.union(
            self._load_ignore_list(os_name=os_name))

        # Get the role dependencies of this component if we haven't
        # done so already
        if self.component_dependencies is None:
            self.component_dependencies = utils.get_role_dependencies(
                self.component_name)
            for i in self.component_dependencies:
                if not os.path.exists(os.path.join(
                        defs.AnsiblePaths.component_tests_directory, i)):
                    print(
                        f"{colours.WARNING}[WARNING] Couldn't find "
                        f"component test directory for dependency: {i}"
                        f"{colours.CLEAR}"
                    )

        # Load the ignore list(s) for the dependencies
        if self.component_dependencies:
            for dependency in self.component_dependencies:
                base_dir = os.path.join(
                    defs.AnsiblePaths.component_tests_directory, dependency)
                # Load the ignore list (either global or OS-specific)
                # for this dependency
                dependency_ignore_list = self._load_ignore_list(
                    os_name=os_name, component_base_dir=base_dir)
                # If the dependency has an ignore list...
                if dependency_ignore_list:
                    # ...add the dependency's ignore list to the final one
                    final_ignore_list = final_ignore_list.union(
                        dependency_ignore_list)

        return final_ignore_list

    def _execute_test(self):
        """
        Runs the component test

        :return: The result of the test; True for success, False for
            failure
        :rtype: bool
        """
        print(f"[STAGE] Initialising {self.component_name} component test")

        # Check if component test directory exists
        if (not os.path.exists(self.component_test_base_dir) or not
                os.path.isdir(self.component_test_base_dir)):
            print(f"{colours.FAIL}Couldn't find {self.component_name} "
                  'directory in '
                  f"{defs.AnsiblePaths.component_tests_directory}"
                  f"{colours.CLEAR}")
            print('Please create a directory for this component and try '
                  'again.')
            return False

        # playbooks_to_run will store the deploy and rollback playbooks
        # with all the test playbooks in between them
        playbooks_to_run = []
        for playbook_file in [defs.AnsiblePaths.component_test_deploy_file,
                              defs.AnsiblePaths.component_test_rollback_file]:
            try:
                playbooks_to_run.append(utils.get_directory_yaml_file(
                    self.component_test_base_dir, playbook_file))
            except FileNotFoundError as err:
                print(f"{colours.FAIL}[ERROR] {err}{colours.CLEAR}")
                print(
                    f"Please create the {playbook_file} playbook file for "
                    f"the {self.component_name} component and try again."
                )
                return False

        # Attempt to find test playbook(s) within the component test
        # directory
        test_playbooks = utils.get_directory_yaml_files(
            self.component_test_base_dir, full_paths=True,
            filename_prefix=defs.AnsiblePaths.component_test_playbook_prefix)

        # The test playbooks will be run in between the deploy and
        # rollback playbooks. The deploy and rollback playbooks will
        # both be run twice to ensure idempotency of the component
        playbooks_to_run = (playbooks_to_run[:1] * 2 + test_playbooks +
                            playbooks_to_run[1:] * 2)

        # Check if the role directory for this component exists
        if not os.path.exists(self.component_role_directory):
            msg = ('Could not find a role directory for this component '
                   f"(at {self.component_role_directory}). You may "
                   'wish to check that this is intended.')
            print(f"{colours.WARNING}[WARNING] {msg}{colours.CLEAR}")
            self.component_role_directory = None

        if not self.os_list:
            self.os_list = []
            for dir_name in os.listdir(
                    defs.AnsiblePaths.os_dockerfiles_directory):
                full_dir_path = os.path.join(
                    defs.AnsiblePaths.os_dockerfiles_directory, dir_name)
                if os.path.isdir(full_dir_path) and os.path.isfile(
                        os.path.join(full_dir_path, 'Dockerfile')):
                    self.os_list.append(dir_name)
        else:
            for os_name in self.os_list:
                full_dir_path = os.path.join(
                    defs.AnsiblePaths.os_dockerfiles_directory, os_name)
                if not os.path.isdir(full_dir_path):
                    print(f"{colours.FAIL}[ERROR] Could not find {os_name} "
                          'directory in '
                          f"{defs.AnsiblePaths.os_dockerfiles_directory}"
                          f"{colours.CLEAR}")
                    return False
                if not os.path.isfile(os.path.join(full_dir_path,
                                                   'Dockerfile')):
                    print(f"{colours.FAIL}[ERROR] {full_dir_path} does not "
                          f"contain a Dockerfile! Please create one and try "
                          f"again.{colours.CLEAR}")
                    return False

        if not self.os_list:
            print(f"{colours.FAIL}[ERROR] Could not find any operating system"
                  f" directories with Dockerfiles in "
                  f"{defs.AnsiblePaths.os_dockerfiles_directory}. Please "
                  f"create at least one directory named after an OS with a "
                  f"Dockerfile within it and try again.{colours.CLEAR}")
            return False

        # Generate the global ignore list (not OS-specific) from this
        # component's ignore list plus the ignore list(s) of any
        # dependencies
        try:
            global_ignore_list = self._generate_ignore_list()
        except utils.CircularRoleDependenciesError as err:
            print(err)
            return False

        # Get the path to the vars.y(a)ml file if it exists
        try:
            path = utils.get_directory_yaml_file(
                self.component_test_base_dir,
                defs.AnsiblePaths.component_test_vars_file)
            vars_path_extra = ['@' + path]
        except FileNotFoundError:
            vars_path_extra = []

        # Run the component setup playbook if it exists
        try:
            component_setup_playbook = utils.get_directory_yaml_file(
                self.component_test_base_dir,
                defs.AnsiblePaths.component_test_setup_file)
        except FileNotFoundError:
            component_setup_playbook = None

        if component_setup_playbook:
            print(f"[STAGE] Run {self.component_name} setup playbook")
            try:
                utils.run_playbook(
                    component_setup_playbook, verbose=self.verbose,
                    extra_vars=f"{defs.TEST_UUID_KEY}={self.uuid} "
                               f"{defs.TEST_DOCKER_NETWORK_KEY}="
                               f"{self.docker_network_name}")
            except subprocess.CalledProcessError:
                print(f"Error when running the {self.component_name} setup "
                      f"playbook ({component_setup_playbook}), please "
                      'check it and try again.')
                return False

        # Run the tests for each defined OS
        for os_name in self.os_list:
            # The name of the Docker container(s) that will be created
            # for this OS
            container_name = (
                f"test-{self.component_name}-{os_name}-{self.uuid}"
            )

            # Load OS-specific ignore lists for this component plus all
            # dependencies
            try:
                os_specific_ignore_list = self._generate_ignore_list(
                    os_name=os_name)
            except utils.CircularRoleDependenciesError as err:
                print(err)
                return False

            # Create container
            print(f"[STAGE] Create {container_name} container")
            try:
                utils.run_playbook(
                    defs.AnsiblePaths.docker_create_playbook,
                    extra_vars=f"container_name={container_name} "
                               f"os={os_name} "
                               f"{defs.TEST_UUID_KEY}={self.uuid} "
                               f"{defs.TEST_DOCKER_NETWORK_KEY}="
                               f"{self.docker_network_name}",
                    verbose=self.verbose,
                    print_command_output=self.verbose)
            except subprocess.CalledProcessError:
                print('[ERROR] Couldn\'t create test Docker container!')
                return False

            try:
                # Run each playbook for the current OS
                for playbook in playbooks_to_run:
                    name_pretty_print = os.path.join(
                        self.component_name, os.path.basename(playbook))
                    print(
                        f"\n[STAGE] Running {name_pretty_print} "
                        f"playbook with {os_name} as the base\n"
                    )
                    utils.run_playbook(
                        playbook,
                        inventory_path=f"{container_name},",
                        extra_vars=([f"container_name={container_name}",
                                     f"{defs.TEST_UUID_KEY}={self.uuid}",
                                     f"{defs.TEST_DOCKER_NETWORK_KEY}="
                                     f"{self.docker_network_name}"] +
                                    vars_path_extra),
                        force_venv_python=False,
                        verbose=self.verbose,
                        user=defs.CONTAINER_TEST_USER
                    )
                if self.test_docker_diff is not None:
                    diff_files = self.test_docker_diff
                else:
                    container = self.docker_client.containers.get(
                        container_name)
                    diff_files = container.diff()
            except subprocess.CalledProcessError:
                return False
            finally:
                # Tear down test Docker container
                if self.pause_before_teardown:
                    input('Press Enter to tear down test container...')
                try:
                    print(f"[STAGE] Tear down container {container_name}")
                    utils.run_playbook(
                        defs.AnsiblePaths.docker_destroy_playbook,
                        extra_vars=f"container_name={container_name} "
                                   f"{defs.TEST_UUID_KEY}={self.uuid} "
                                   f"{defs.TEST_DOCKER_NETWORK_KEY}="
                                   f"{self.docker_network_name}",
                        verbose=self.verbose,
                        print_command_output=self.verbose
                    )
                except subprocess.CalledProcessError as err:
                    print('Error when tearing down container!')
                    raise err

            print('[STAGE] Report file changes from container')
            added_files = []
            deleted_files = []
            for diff_file in diff_files:
                if (diff_file[defs.Docker.API_DIFF_KIND_KEY] ==
                        defs.Docker.API_DIFF_ADDED_VALUE):
                    file_path = diff_file['Path']
                    on_ignore_list = (
                        self._is_file_on_ignore_list(
                            file_path,
                            global_ignore_list)
                        or
                        self._is_file_on_ignore_list(
                            file_path,
                            os_specific_ignore_list)
                    )
                    if not on_ignore_list:
                        added_files.append(file_path)
                elif (diff_file[defs.Docker.API_DIFF_KIND_KEY] ==
                        defs.Docker.API_DIFF_DELETED_VALUE):
                    file_path = diff_file['Path']
                    on_ignore_list = (
                        self._is_file_on_ignore_list(
                            file_path,
                            global_ignore_list)
                        or
                        self._is_file_on_ignore_list(
                            file_path,
                            os_specific_ignore_list)
                    )
                    if not on_ignore_list:
                        deleted_files.append(file_path)
                elif (diff_file[defs.Docker.API_DIFF_KIND_KEY] ==
                        defs.Docker.API_DIFF_MODIFIED_VALUE):
                    # For now, just ignore modified files
                    pass
            if added_files:
                print('[Files that were created but weren\'t deleted]')
                for i in added_files:
                    print(i)
            if deleted_files:
                print('[Files that were deleted]')
                for i in deleted_files:
                    print(i)
            if added_files or deleted_files:
                print(f"{colours.FAIL}Changes for files that weren't on "
                      'the ignore list occurred. Please see the output '
                      'above and either modify the component role to deal'
                      ' with these files or add them to the ignore list.'
                      f"{colours.CLEAR}")
                return False
            else:
                print(f"{colours.SUCCESS}No unexpected file changes "
                      f"occurred{colours.CLEAR}")

        return True

    def _teardown_test(self):
        """
        Tears down the component test: runs the component teardown
        playbook, if it exists.

        :return: True on success, False on failure
        :rtype: bool
        """
        # Run the component teardown playbook if it exists
        try:
            component_teardown_playbook = utils.get_directory_yaml_file(
                self.component_test_base_dir,
                defs.AnsiblePaths.component_test_teardown_file)
        except FileNotFoundError:
            component_teardown_playbook = None

        if component_teardown_playbook:
            print(f"[STAGE] Run {self.component_name} teardown playbook")
            try:
                utils.run_playbook(
                    component_teardown_playbook, verbose=self.verbose,
                    extra_vars=f"{defs.TEST_UUID_KEY}={self.uuid} "
                               f"{defs.TEST_DOCKER_NETWORK_KEY}="
                               f"{self.docker_network_name}")
            except subprocess.CalledProcessError:
                print(f"Error when running the {self.component_name} teardown"
                      f" playbook ({component_teardown_playbook}), please "
                      'check it and try again.')
                return False
